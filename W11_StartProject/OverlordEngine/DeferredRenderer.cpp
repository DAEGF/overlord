#include "stdafx.h"
#include "DeferredRenderer.h"
#include "OverlordGame.h"
#include "RenderTarget.h"
#include <minwinbase.h>


DeferredRenderer::~DeferredRenderer()
{
	for(int i= 0; i < BUFFER_COUNT;++i)
	{
		SafeRelease(m_pBuffers[i]);		
		SafeRelease(m_pRenderTargetViews[i]);
		SafeRelease(m_pShaderResourceViews[i]);
	}
}

void DeferredRenderer::Begin() const
{
	if(!m_Initialized) Logger::LogError(L"YO, DeferredRenderer::Begin() > renderer not initialized");

	ClearRenderTargets();

	SetRenderTargets();

	//Render Scene

	//Fill G-Buffers

}

void DeferredRenderer::End(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void DeferredRenderer::InitRenderer(ID3D11DeviceContext* pDeviceContext,ID3D11Device*  pDevice, OverlordGame* pGame)
{
	if(m_Initialized) return;

	m_pDevice = pDevice;
	m_pDeviceContext =pDeviceContext;
	m_pGame = pGame;

	int w = m_pGame->GetGameSettings().Window.Width;
	int h = m_pGame->GetGameSettings().Window.Height;

	//creat buffers
	CreatBuffersAndViews(w,h,DXGI_FORMAT_B8G8R8A8_UNORM,DeferredBufferIds::LIGHT_ACCUMULATION);
	CreatBuffersAndViews(w,h,DXGI_FORMAT_B8G8R8A8_UNORM,DeferredBufferIds::DIFFUSE);
	CreatBuffersAndViews(w,h,DXGI_FORMAT_B8G8R8A8_UNORM,DeferredBufferIds::SPECULAR);
	CreatBuffersAndViews(w,h,DXGI_FORMAT_R32G32B32A32_FLOAT,DeferredBufferIds::NORMAL);

	//set Defaults 
	auto rt = pGame->GetRenderTarget();
	m_pDefaultRenderTargetView = rt->GetRenderTargetView();
	m_pDefaultDepthStencilView = rt->GetDepthStencilView();
	m_pDefaultStencilViewSVR = rt->GetDepthShaderResourceView();

	//create and init deferred material
	//...

	m_Initialized = true;

}

void DeferredRenderer::CreatBuffersAndViews(int width, int height, DXGI_FORMAT format, DeferredBufferIds id)
{
	// TEXTURE BUFFER
	D3D11_TEXTURE2D_DESC bufferDesc;
	ZeroMemory(&bufferDesc,sizeof(bufferDesc));

	bufferDesc.Width = width;
	bufferDesc.Height = height;
	bufferDesc.MipLevels = 1;
	bufferDesc.ArraySize = 1;
	bufferDesc.Format = format;
	bufferDesc.SampleDesc.Count = 1;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;

	//Create Resource
	auto hResult = m_pDevice->CreateTexture2D(&bufferDesc, nullptr, &m_pBuffers[int(id)]);
    Logger::LogHResult(hResult, L"DeferredRenderer::CreateBuffers(...)");

	//rendertarget views

	D3D11_RENDER_TARGET_VIEW_DESC viewDesc;
	ZeroMemory(&viewDesc,sizeof(viewDesc));

	viewDesc.Format = format;
	viewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	viewDesc.Texture2D.MipSlice = 0;

	hResult = m_pDevice->CreateRenderTargetView(m_pBuffers[int(id)],&viewDesc,&m_pRenderTargetViews[int(id)]);
	Logger::LogHResult(hResult, L"DeferredRenderer::CreateBuffers(...)");


	//shader resource views


	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc,sizeof(srvDesc));

	srvDesc.Format = format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;

	
	hResult = m_pDevice->CreateShaderResourceView(m_pBuffers[int(id)],&srvDesc,&m_pShaderResourceViews[int(id)]);
	Logger::LogHResult(hResult, L"DeferredRenderer::CreateBuffers(...)");
}

void DeferredRenderer::ClearRenderTargets() const
{
	float clearColor[4] = {0.f,0.f,0.f,1.f};
	for(int i= 0; i < BUFFER_COUNT;++i)
	{
		m_pDeviceContext->ClearRenderTargetView(m_pRenderTargetViews[i],clearColor);
	}
	m_pDeviceContext->ClearDepthStencilView(m_pDefaultDepthStencilView,D3D11_CLEAR_STENCIL,1.0f,0);
}

void DeferredRenderer::SetRenderTargets() const
{
	m_pDeviceContext->OMSetRenderTargets(BUFFER_COUNT, m_pRenderTargetViews,m_pDefaultDepthStencilView);
}
