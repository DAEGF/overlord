#pragma once
#include "Singleton.h"

class OverlordGame;

enum class DeferredBufferIds
{
	LIGHT_ACCUMULATION = 0,
	DIFFUSE = 1,
	SPECULAR = 2,
	NORMAL = 3
};

class DeferredRenderer final : public Singleton<DeferredRenderer>
{
public:
	virtual ~DeferredRenderer();

	void Begin() const;
	void End(const GameContext& gameContext);

	void InitRenderer(ID3D11DeviceContext* pDeviceContext,ID3D11Device*  pDevice, OverlordGame* pGame);
	ID3D11ShaderResourceView* GetShaderResourceView(DeferredBufferIds id) const { return m_pShaderResourceViews[int(id)]; }

private:
	bool m_Initialized;


	ID3D11Device*  m_pDevice = nullptr;
	ID3D11DeviceContext* m_pDeviceContext = nullptr;
	OverlordGame* m_pGame = nullptr;


	static const int BUFFER_COUNT = 4;
	ID3D11ShaderResourceView* m_pShaderResourceViews[BUFFER_COUNT] = {nullptr};
	ID3D11Texture2D* m_pBuffers[BUFFER_COUNT] = {nullptr};
	ID3D11RenderTargetView* m_pRenderTargetViews[BUFFER_COUNT] = {nullptr};

	ID3D11RenderTargetView* m_pDefaultRenderTargetView = nullptr; //FROM GAME
	ID3D11DepthStencilView* m_pDefaultDepthStencilView = nullptr; //FROM GAME
	ID3D11ShaderResourceView* m_pDefaultStencilViewSVR = nullptr; // FROM GAME

	void CreatBuffersAndViews(int width, int height, DXGI_FORMAT format, DeferredBufferIds id );
	void ClearRenderTargets() const;
	void SetRenderTargets() const;
};

