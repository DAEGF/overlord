//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "QuadDrawerTestScene.h"
#include <ContentManager.h>
#include <QuadDrawer.h>
#include <TextureData.h>

QuadDrawerTestScene::QuadDrawerTestScene() :
	GameScene(L"QuadDrawerTestScene")
{}

void QuadDrawerTestScene::Initialize()
{
	m_pTexture = ContentManager::Load<TextureData>(L"Resources/Textures/Chair_Dark.dds");
}

void QuadDrawerTestScene::Update()
{}

void QuadDrawerTestScene::Draw()
{
	const auto gameContext = GetGameContext();
	QuadDrawer::GetInstance()->Draw(gameContext, m_pTexture->GetShaderResourceView(), { 10,10,500,300 });
}

void QuadDrawerTestScene::SceneActivated()
{}

void QuadDrawerTestScene::SceneDeactivated()
{}
