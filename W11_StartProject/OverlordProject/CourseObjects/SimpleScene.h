#pragma once
#include "GameScene.h"

class TextureData;

class SimpleScene final : public GameScene
{
public:
	SimpleScene();
	~SimpleScene() = default;

	SimpleScene(const SimpleScene& other) = delete;
	SimpleScene(SimpleScene&& other) noexcept = delete;
	SimpleScene& operator=(const SimpleScene& other) = delete;
	SimpleScene& operator=(SimpleScene&& other) noexcept = delete;

	void Initialize() override;
	void Update() override;
	void Draw() override;
	void SceneActivated() override;
	void SceneDeactivated() override;
};

