//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "SimpleScene.h"
#include "ContentManager.h"
#include "GameObject.h"
#include "Components.h"
#include "../Materials/BasicMaterial_Deferred.h"

SimpleScene::SimpleScene() :
	GameScene(L"SimpleScene")
{}

void SimpleScene::Initialize()
{
	const auto gameContext = GetGameContext();

	auto mat = new BasicMaterial_Deferred();
	gameContext.pMaterialManager->AddMaterial(mat, 0);
	mat->SetLightDirection({ -0.5f,-0.5f,0.5f });
	mat->SetColorDiffuse({ 1,0,0,1 });
	mat->SetColorSpecular({ 1,1,1,1 });

	auto gameObj = new GameObject();
	auto mdlCmp = new ModelComponent(L"Resources/Meshes/UnitSphere.ovm");
	mdlCmp->SetMaterial(0);
	gameObj->AddComponent(mdlCmp);
	gameObj->GetTransform()->Scale(10, 10, 10);
	AddChild(gameObj);

	gameObj = new GameObject();
	mdlCmp = new ModelComponent(L"Resources/Meshes/UnitSphere.ovm");
	mdlCmp->SetMaterial(0);
	gameObj->AddComponent(mdlCmp);
	gameObj->GetTransform()->Scale(10, 10, 10);
	gameObj->GetTransform()->Translate(20, 0, 20);
	AddChild(gameObj);

	gameObj = new GameObject();
	mdlCmp = new ModelComponent(L"Resources/Meshes/UnitSphere.ovm");
	mdlCmp->SetMaterial(0);
	gameObj->AddComponent(mdlCmp);
	gameObj->GetTransform()->Scale(10, 10, 10);
	gameObj->GetTransform()->Translate(-20, -20, 0);
	AddChild(gameObj);
}

void SimpleScene::Update()
{}

void SimpleScene::Draw()
{}

void SimpleScene::SceneActivated()
{}

void SimpleScene::SceneDeactivated()
{}
