#pragma once
#include "GameScene.h"

class TextureData;

class QuadDrawerTestScene final : public GameScene
{
public:
	QuadDrawerTestScene();
	~QuadDrawerTestScene() = default;

	QuadDrawerTestScene(const QuadDrawerTestScene& other) = delete;
	QuadDrawerTestScene(QuadDrawerTestScene&& other) noexcept = delete;
	QuadDrawerTestScene& operator=(const QuadDrawerTestScene& other) = delete;
	QuadDrawerTestScene& operator=(QuadDrawerTestScene&& other) noexcept = delete;

	void Initialize() override;
	void Update() override;
	void Draw() override;
	void SceneActivated() override;
	void SceneDeactivated() override;

private:
	TextureData* m_pTexture = nullptr;
};

