//**************//
// BASIC EFFECT //
//**************//

/*
	- Diffuse Color/Texture
	- Specular Color
	- SpecularLevel Texture (Blinn)
	- Specular Intensity (Shininess)
	- NormalMap Texture
	- Ambient Color
	- Ambient Intensity [0-1]
	- Opacity Texture/Value [0-1]
*/

//GLOBAL MATRICES
//***************
// The World View Projection Matrix
float4x4 gMatrixWorldViewProj : WORLDVIEWPROJECTION;
// The ViewInverse Matrix - the third row contains the camera position!
float4x4 gMatrixViewInv : VIEWINVERSE;
// The World Matrix
float4x4 gMatrixWorld : WORLD;

//STATES
//******
RasterizerState gRasterizerState 
{ 
	FillMode = SOLID;
	CullMode = BACK; 
};

BlendState gEnableBlending 
{     
	BlendEnable[0] = TRUE;
	SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA;
};

BlendState gDisableBlending 
{     
	BlendEnable[0] = FALSE;
	SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA;
};

DepthStencilState gDepthState
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

//SAMPLER STATES
//**************
SamplerState gTextureSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	//Filter = ANISOTROPIC;
 	AddressU = WRAP;
	AddressV = WRAP;
	AddressW = WRAP;
};

//LIGHT
//*****
float3 gLightDirection:DIRECTION
<
	string UIName = "Light Direction";
	string Object = "TargetLight";
> = float3(0.577f, 0.577f, 0.577f);

//DIFFUSE
//*******
bool gUseDiffuseTexture
<
	string UIName = "Diffuse Texture";
	string UIWidget = "Bool";
> = false;

float4 gColorDiffuse
<
	string UIName = "Diffuse Color";
	string UIWidget = "Color";
> = float4(1,1,1,1);

Texture2D gTextureDiffuse
<
	string UIName = "Diffuse Texture";
	string UIWidget = "Texture";
>;

//SPECULAR
//********
float4 gColorSpecular
<
	string UIName = "Specular Color";
	string UIWidget = "Color";
> = float4(1,1,1,1);

Texture2D gTextureSpecularLevel
<
	string UIName = "Specular Level Texture";
	string UIWidget = "Texture";
>;

bool gUseSpecularLevelTexture
<
	string UIName = "Specular Level Texture";
	string UIWidget = "Bool";
> = false;

int gShininess<
	string UIName = "Shininess";
	string UIWidget = "Slider";
	float UIMin = 1;
	float UIMax = 100;
	float UIStep = 0.1f;
> = 15;

//AMBIENT
//*******
float4 gColorAmbient
<
	string UIName = "Ambient Color";
	string UIWidget = "Color";
> = float4(0,0,0,1);

float gAmbientIntensity
<
	string UIName = "Ambient Intensity";
	string UIWidget = "slider";
	float UIMin = 0;
	float UIMax = 1;
>  = 0.0f;

//NORMAL MAPPING
//**************
bool gFlipGreenChannel
<
	string UIName = "Flip Green Channel";
	string UIWidget = "Bool";
> = false;

bool gUseNormalMapping
<
	string UIName = "Normal Mapping";
	string UIWidget = "Bool";
> = false;

Texture2D gTextureNormal
<
	string UIName = "Normal Texture";
	string UIWidget = "Texture";
>;

//OPACITY
//***************
float gOpacityLevel<
	string UIName = "Opacity";
	string UIWidget = "slider";
	float UIMin = 0;
	float UIMax = 1;
>  = 1.0f;

bool gUseOpacityTexture
<
	string UIName = "Opacity Map";
	string UIWidget = "Bool";
> = false;

Texture2D gTextureOpacity
<
	string UIName = "Opacity Map";
	string UIWidget = "Texture";
>;

//VS IN & OUT
//***********
struct VS_Input
{
	float3 Position: POSITION;
	float3 Normal: NORMAL;
	float3 Tangent: TANGENT;
	float2 TexCoord: TEXCOORD0;
};

struct VS_Output
{
	float4 Position: SV_POSITION;
	float3 Normal: NORMAL;
	float3 Tangent: TANGENT;
	float2 TexCoord: TEXCOORD0;
    float3 Binormal : BINORMAL;
};

struct PS_OUTPUT
{
    float4 LightAccumulation : SV_TARGET0;
    float4 Diffuse : SV_TARGET1;
    float4 Specular : SV_TARGET2;
    float4 Normal : SV_TARGET3;
};

// The main vertex shader
VS_Output MainVS(VS_Input input) {
	
	VS_Output output = (VS_Output)0;
	
	output.Position = mul(float4(input.Position, 1.0), gMatrixWorldViewProj);
	output.Normal = normalize(mul(input.Normal, (float3x3)gMatrixWorld));
	output.Tangent = normalize(mul(input.Tangent, (float3x3)gMatrixWorld));
	output.TexCoord = input.TexCoord;
	
    float3 binormal = cross(output.Tangent, output.Normal);
    binormal = normalize(binormal);
    if (gFlipGreenChannel)
        binormal = -binormal;

    output.Binormal = binormal;
	return output;
}

float4 DoNormalMapping(float3x3 TBN, Texture2D normalMap, float2 uv)
{
    float3 normal = normalMap.Sample(gTextureSampler, uv).xyz;
    normal = normal * 2.0f - 1.0f;

    normal = mul(normal,TBN);
    return normalize(float4(normal, 0));
}

// The main pixel shader
PS_OUTPUT MainPS(VS_Output input)
{
    PS_OUTPUT output = (PS_OUTPUT)0;

    //Diffuse
    float4 diffuse = gColorDiffuse;
    if (gUseDiffuseTexture)
        diffuse *= gTextureDiffuse.Sample(gTextureSampler, input.TexCoord);
    output.Diffuse = diffuse;

    //ambient
    float4 ambient = gColorAmbient;
    ambient *= diffuse;
    ambient *= gAmbientIntensity;
    output.LightAccumulation = ambient;

    //normal
    float4 normal;
    if(gUseNormalMapping)
    {
        float3x3 TBN = float3x3(
        normalize(input.Tangent),
        normalize(input.Binormal),
        normalize(input.Normal)
        );
        normal = DoNormalMapping(TBN, gTextureNormal, input.TexCoord);

    }
    else
    {
        normal = normalize(float4(input.Normal, 0));
    }

    input.Normal = normal.xyz;

    //Specular
    float3 specularLevel = gColorSpecular.xyz;
    if(gUseSpecularLevelTexture)
        specularLevel = gTextureSpecularLevel.Sample(gTextureSampler, input.TexCoord).rgb;
    output.Specular = float4(specularLevel, log2(gShininess / 10.5f));

    return output;

}

// Default Technique
technique10 WithoutAlphaBlending {
	pass p0 {
		SetDepthStencilState(gDepthState, 0);
		SetRasterizerState(gRasterizerState);
		SetBlendState(gDisableBlending,float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		
		SetVertexShader(CompileShader(vs_4_0, MainVS()));
		SetGeometryShader( NULL );
		SetPixelShader(CompileShader(ps_4_0, MainPS()));
	}
}

