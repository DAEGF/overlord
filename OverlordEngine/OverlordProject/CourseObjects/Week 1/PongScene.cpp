#include "stdafx.h"
#include "PongScene.h"
#include "PhysxProxy.h"
#include "PhysxManager.h"
#include "CubePrefab.h"
#include "Components.h"
#include "SpherePrefab.h"

PongScene::PongScene()
	:GameScene(L"PongScene"),
	m_pBall{},
	m_pPaddleOne{},
	m_pPaddleTwo{}
{

}

void PongScene::Initialize()
{
	GetPhysxProxy()->EnablePhysxDebugRendering(true);
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto pBouncyMaterial = pPhysx->createMaterial(0.5f, 0.5f, 1.0f);
	auto pGameContext = GetGameContext();

	//Ground
	auto pGround = new GameObject();
	pGround->AddComponent(new RigidBodyComponent(true));
	std::shared_ptr<physx::PxGeometry> pGeometry(new physx::PxPlaneGeometry());
	pGround->AddComponent(new ColliderComponent(pGeometry, *pBouncyMaterial,
		physx::PxTransform(physx::PxQuat(DirectX::XM_PIDIV2, physx::PxVec3(0, 0, 1)))));
	AddChild(pGround);

	//Bounds
		
	//Paddle 1
	m_pPaddleOne = new CubePrefab(2, 1, 5, static_cast<DirectX::XMFLOAT4>(DirectX::Colors::Red));
	
	auto pRigidBody = new RigidBodyComponent();
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group0);
	pRigidBody->SetKinematic(true);
	m_pPaddleOne->AddComponent(pRigidBody);

	std::shared_ptr<physx::PxGeometry> pBoxGeometry(
		new physx::PxBoxGeometry(physx::PxReal(1), physx::PxReal(0.5), physx::PxReal(2.5)));
	m_pPaddleOne->AddComponent(new ColliderComponent(pBoxGeometry, *pBouncyMaterial));
	m_pPaddleOne->GetTransform()->Translate(-10, 1, 0);
	
	AddChild(m_pPaddleOne);
	
	//Paddle 2
	m_pPaddleTwo = new CubePrefab(2, 1, 5, static_cast<DirectX::XMFLOAT4>(DirectX::Colors::Red));

	pRigidBody = new RigidBodyComponent();
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group0);
	pRigidBody->SetKinematic(true);
	m_pPaddleTwo->AddComponent(pRigidBody);
	
	m_pPaddleTwo->AddComponent(new ColliderComponent(pBoxGeometry, *pBouncyMaterial));

	m_pPaddleTwo->GetTransform()->Translate(10, 1, 0);
	AddChild(m_pPaddleTwo);

	//BALL
	m_pBall = new SpherePrefab();
	m_pBall->GetTransform()->Translate(0, 1, 0);

	pRigidBody = new RigidBodyComponent();
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group0);
	m_pBall->AddComponent(pRigidBody);

	std::shared_ptr<physx::PxGeometry> pSphereGeometry(new physx::PxSphereGeometry(1));
	m_pBall->AddComponent(new ColliderComponent(pSphereGeometry, *pBouncyMaterial));
	AddChild(m_pBall);


	//INPUTS
	pGameContext.pInput->AddInputAction(InputAction(int(UpP1), InputTriggerState::Down, 'W'));
	pGameContext.pInput->AddInputAction(InputAction(int(DownP1), InputTriggerState::Down, 'S'));
	pGameContext.pInput->AddInputAction(InputAction(int(UpP2), InputTriggerState::Down, 'I'));
	pGameContext.pInput->AddInputAction(InputAction(int(DownP2), InputTriggerState::Down, 'K'));

}

void PongScene::Draw()
{
}

void PongScene::Update()
{
	auto pGameContext = GetGameContext();
	if (pGameContext.pInput->IsActionTriggered(UpP1))
	{
		m_pPaddleOne->GetTransform()->Translate(
			m_pPaddleOne->GetTransform()->GetPosition().x,
			m_pPaddleOne->GetTransform()->GetPosition().y,
			m_pPaddleOne->GetTransform()->GetPosition().z);
	}
	else if (pGameContext.pInput->IsActionTriggered(DownP1))
	{

	}

	if (pGameContext.pInput->IsActionTriggered(UpP2))
	{

	}
	else if (pGameContext.pInput->IsActionTriggered(DownP2))
	{

	}
}
