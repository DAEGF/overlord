#include "stdafx.h"
#include "ComponentTestScene.h"
#include "PhysxProxy.h"
#include "PhysxManager.h"
#include "GameObject.h"
#include "Components.h"
#include "SpherePrefab.h"

ComponentTestScene::ComponentTestScene()
	:GameScene(L"ComponentTestScene"),
	m_pSphere{}
{

}

void ComponentTestScene::Initialize()
{
	GetPhysxProxy()->EnablePhysxDebugRendering(true);
	auto pPhysx = PhysxManager::GetInstance()->GetPhysics();
	auto pBouncyMaterial = pPhysx->createMaterial(0.5f, 0.5f, 1.0f);

	//Ground
	auto pGround = new GameObject();
	pGround->AddComponent(new RigidBodyComponent(true));
	std::shared_ptr<physx::PxGeometry> pGeometry(new physx::PxPlaneGeometry());
	pGround->AddComponent(new ColliderComponent(pGeometry, *pBouncyMaterial, 
		physx::PxTransform(physx::PxQuat(DirectX::XM_PIDIV2, physx::PxVec3(0, 0, 1)))));
	AddChild(pGround);

	//SPHERE 
	auto pSphere = new SpherePrefab();
	pSphere->GetTransform()->Translate(0, 30, 0);

	auto pRigidBody = new RigidBodyComponent();
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group0);
	pRigidBody->SetCollisionIgnoreGroups(static_cast<CollisionGroupFlag>(CollisionGroupFlag::Group1 | CollisionGroupFlag::Group2));
	pSphere->AddComponent(pRigidBody);

	std::shared_ptr<physx::PxGeometry> pSphereGeometry(new physx::PxSphereGeometry(1));
	pSphere->AddComponent(new ColliderComponent(pSphereGeometry, *pBouncyMaterial));
	m_pSphere = pSphere;
	AddChild(m_pSphere);


	//SPHERE 2
	pSphere = new SpherePrefab();
	pSphere->GetTransform()->Translate(0, 20, 0);

	pRigidBody = new RigidBodyComponent();
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group1);
	pSphere->AddComponent(pRigidBody);

	pSphere->AddComponent(new ColliderComponent(pSphereGeometry, *pBouncyMaterial));

	AddChild(pSphere);

	//SPHERE 3
	pSphere = new SpherePrefab();
	pSphere->GetTransform()->Translate(0, 10, 0);

	pRigidBody = new RigidBodyComponent();
	pRigidBody->SetCollisionGroup(CollisionGroupFlag::Group2);
	pSphere->AddComponent(pRigidBody);

	pSphere->AddComponent(new ColliderComponent(pSphereGeometry, *pBouncyMaterial));

	AddChild(pSphere);

	//INPUT
	const auto pGameContext = GetGameContext();
	auto inputAction = InputAction(0, InputTriggerState::Down, 'M');
	pGameContext.pInput->AddInputAction(inputAction);
}

void ComponentTestScene::Draw()
{
}

void ComponentTestScene::Update()
{
	const auto pGameContext = GetGameContext();
	if (pGameContext.pInput->IsActionTriggered(0))
	{
		m_pSphere->GetComponent<RigidBodyComponent>()->AddForce(physx::PxVec3(0, 1, 0)); 
	}
}
