#pragma once
#include "GameScene.h"

class PongScene final : public GameScene
{
	enum Controls
	{
		UpP1, DownP1, UpP2, DownP2
	};

public:
	PongScene();
	~PongScene() = default;

	PongScene(const PongScene& other) = delete;
	PongScene(PongScene&& other) = delete;
	PongScene& operator=(const PongScene& other) = delete;
	PongScene& operator=(PongScene&& other) = delete;

protected:
	virtual void Initialize() override;
	virtual void Draw() override;
	virtual void Update() override;

private:
	GameObject* m_pBall;
	GameObject* m_pPaddleOne;
	GameObject* m_pPaddleTwo;
};