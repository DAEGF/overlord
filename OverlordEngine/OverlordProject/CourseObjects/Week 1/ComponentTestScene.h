#pragma once
#include "GameScene.h"

class ComponentTestScene final : public GameScene
{
public:
	ComponentTestScene();
	~ComponentTestScene() = default;

	ComponentTestScene(const ComponentTestScene& other) = delete;
	ComponentTestScene(ComponentTestScene&& other) = delete;
	ComponentTestScene& operator=(const ComponentTestScene& other) = delete;
	ComponentTestScene& operator=(ComponentTestScene&& other) = delete;

protected:
	virtual void Initialize() override;
	virtual void Draw() override;
	virtual void Update() override;

private:
	GameObject* m_pSphere;
};