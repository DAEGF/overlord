#pragma once
#include "GameObject.h"

class ControllerComponent;
class ModelComponent;

 /*Idle, Run, Walk, Alert, Bite, Die, Stagger, Sting*/
class EnemyBug :
	public GameObject
{
	enum Animation : UINT
	{
		Idle,
		Run,
		Walk,
		Alert,
		Bite,
		Die,
		Stagger,
		Sting
	};

	enum AIState
	{
		PATROL,
		CHASING
	};

public:
	EnemyBug();
	virtual ~EnemyBug() = default;

	EnemyBug(const EnemyBug& other) = delete;
	EnemyBug(EnemyBug&& other) noexcept = delete;
	EnemyBug& operator=(const EnemyBug& other) = delete;
	EnemyBug& operator=(EnemyBug&& other) noexcept = delete;

	void Initialize(const GameContext& gameContext) override;
	void PostInitialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;

	void TakeDamage(int damage);
	void SetPatrolPoint(XMFLOAT3 pos);
private:
	XMFLOAT3 HandleAIState();
	void Move(XMFLOAT3 target, const GameContext& gameContext);
	void SetAnimation();

	ControllerComponent* m_pController;
	ModelComponent* m_pModel;
	GameObject* m_pTarget;

	GameObject* m_pSensorHitBox;
	GameObject* m_pBiteHitBox;
	XMFLOAT3 m_Velocity;

	std::vector<XMFLOAT3> m_PatrolCheckPoints;

	int m_Health;
	unsigned int m_IdxSelected;
	AIState m_AIState;
	Animation m_Animation;

	FMOD::Sound* m_pHurt;
	FMOD::Sound* m_pDeath;
	FMOD::Channel* m_pChannel;
};





