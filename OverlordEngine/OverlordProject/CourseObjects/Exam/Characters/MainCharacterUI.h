#pragma once
#include "GameObject.h"

class MainCharacterUI : public GameObject
{
public:
	MainCharacterUI();
	virtual ~MainCharacterUI() = default;

	MainCharacterUI(const MainCharacterUI& other) = delete;
	MainCharacterUI(MainCharacterUI&& other) noexcept = delete;
	MainCharacterUI& operator=(const MainCharacterUI& other) = delete;
	MainCharacterUI& operator=(MainCharacterUI&& other) noexcept = delete;

	void Initialize(const GameContext& gameContext) override;
	void PostInitialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;

	void ShowHud();
	void SetHealthMax(int healthMax) {m_HealthMax = healthMax;}
	void SetHealth(int health) { m_Health = health; }
	void SetAmountStarsInLevel(int amountStars) {m_AmountStars = amountStars;}
	void SetAmountStarsFound(int found) {m_AmountStarsFound = found;}
	void SetLives(int lives) { m_Lives = lives;}
	void SetLivesMax(int max) {m_LivesMax = max;}

private:
	bool m_ShowingHud;
	std::vector<GameObject*> m_HealthIcons;
	std::vector<GameObject*> m_StarIcons;
	std::vector<GameObject*> m_pHealthBgs;
	
	int m_HealthMax, m_Health, m_AmountStars, m_AmountStarsFound, m_Lives, m_LivesMax;
	float m_HideHeight, m_ShowHeight;

	float m_HudShowTimer, m_HudShowTimerMax;
};

