#include "stdafx.h"
#include "Star.h"
#include "ModelComponent.h"
 
#include "../Scenes/PrototypeScene.h"
#include "ParticleEmitterComponent.h"
#include "ColliderComponent.h"
#include "RigidBodyComponent.h"
#include "PhysxManager.h"
#include "../../../Materials/Shadow/DiffuseMaterial_Shadow.h"
#include "TransformComponent.h"
#include "MainCharacter.h"
#include "SoundManager.h"
#include "TimerHandler.h"


Star::Star(): 
	m_pCollect(nullptr), m_pChannel(nullptr), 
	m_PickedUp(false), 
	m_TotalYaw(0), 
	m_DeathTimer(0), m_DeathTimerMax(2.f)
{
}

void Star::Initialize(const GameContext& gameContext)
{
	(gameContext);
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto bouncyMaterial = physX->createMaterial(0.5, 0.5, 1.0f);
	SetTag(L"Star");

	auto model = new ModelComponent(L"Resources/Meshes/Star.ovm");
	model->SetMaterial(PrototypeScene::Material::StarMat);
	AddComponent(model);

	auto particleEmitter = new ParticleEmitterComponent(L"./Resources/Textures/StarHitParticle.png", 10);
	particleEmitter->SetVelocity(XMFLOAT3(1.f, 3.f, 1.f));
	particleEmitter->SetMinSize(.5f);
	particleEmitter->SetMaxSize(1.0f);
	particleEmitter->SetMinEnergy(.50f);
	particleEmitter->SetMaxEnergy(1.0f);
	particleEmitter->SetMinSizeGrow(-1.f);
	particleEmitter->SetMaxSizeGrow(1.f);
	particleEmitter->SetMinEmitterRange(2.f);
	particleEmitter->SetMaxEmitterRange(3.f);
	particleEmitter->SetColor(XMFLOAT4(1.f, 1.f, 0.f, 1.f));
	particleEmitter->SetIsLooping(true);
	AddComponent(particleEmitter);

	auto rigidBody = new RigidBodyComponent(false);
	rigidBody->SetKinematic(true);
	AddComponent(rigidBody);

	std::shared_ptr<physx::PxGeometry> geom(new physx::PxBoxGeometry(2.5f,2.5f,1.f));
	auto collider = new ColliderComponent(geom, *bouncyMaterial, physx::PxTransform(physx::PxQuat(DirectX::XM_PIDIV2, physx::PxVec3(0, 0, 1))));
	collider->EnableTrigger(true);
	AddComponent(collider);

	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->createSound("./Resources/Sounds/CollectStarSFX.wav", (FMOD_3D | FMOD_DEFAULT), NULL, &m_pCollect);

	GetTransform()->Scale(.2f,.2f,.2f);
}

void Star::PostInitialize(const GameContext& gameContext)
{
	(gameContext);
	SetOnTriggerCallBack([this](GameObject* trigger, GameObject* other, TriggerAction action)
	{
		(action);
		(trigger);
		if(m_PickedUp) return;
		Logger::Log(Warning,L"Triggered");
		if(other->GetTag() == L"Player")
		{
			reinterpret_cast<MainCharacter*>(other)->CollectStar();
			PickUp();
		}
		
	});
} 

void Star::Update(const GameContext& gameContext)
{
	(gameContext);
	if(m_PickedUp)
	{
		m_TotalYaw += 1000.f * gameContext.pGameTime->GetElapsed();
		GetTransform()->Displace(0,50.f * gameContext.pGameTime->GetElapsed(),0);
		if(UpdateAndCheckTimer(m_DeathTimer,m_DeathTimerMax,gameContext.pGameTime->GetElapsed())) SetDeleteFlag(true);
	}
	else m_TotalYaw += 100.f * gameContext.pGameTime->GetElapsed();

	GetTransform()->Rotate(0,m_TotalYaw,0);
}

void Star::PickUp()
{
	m_PickedUp = true;
	SoundManager::GetInstance()->GetSystem()->playSound(m_pCollect, 0, false, &m_pChannel);
}

