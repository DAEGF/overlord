#include "stdafx.h"
#include "EnemyBug.h"
#include "PhysxManager.h"
#include "Components.h"
#include "ModelAnimator.h"
#include "../../../Materials/Shadow/SkinnedDiffuseMaterial_Shadow.h"
#include "../Scenes/PrototypeScene.h"
#include "MainCharacter.h"
#include "SoundManager.h"

EnemyBug::EnemyBug(): 
	m_pController(nullptr), m_pModel(nullptr), m_pTarget(nullptr), 
	m_pSensorHitBox(nullptr),m_pBiteHitBox(nullptr),
	m_Velocity(), m_Health(2), m_IdxSelected(0), m_AIState(PATROL), m_Animation(), 
	m_pHurt(nullptr), m_pDeath(nullptr), m_pChannel(nullptr)
{
}


void EnemyBug::Initialize(const GameContext& gameContext)
{
	(gameContext);
	SetTag(L"Bug");
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto pBouncyMaterial = physX->createMaterial(.5, .5, 1);

	m_pController = new ControllerComponent(pBouncyMaterial,5.f,2);
	AddComponent(m_pController);



	auto rotatedModel  = new GameObject();
	m_pModel = new ModelComponent(L"Resources/Meshes/Bug.ovm");
	rotatedModel->AddComponent(m_pModel);
	rotatedModel->GetTransform()->Rotate(0,180,0);
	m_pModel->SetMaterial(PrototypeScene::Material::BugMat);
	AddChild(rotatedModel);

	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->createSound("./Resources/Sounds/HurtInsectSFX.mp3", (FMOD_3D | FMOD_DEFAULT), NULL, &m_pHurt);
	fmodSystem->createSound("./Resources/Sounds/DeathInsectSFX.mp3", (FMOD_3D | FMOD_DEFAULT), NULL, &m_pDeath);


	m_pBiteHitBox = new GameObject();
	//auto child = new GameObject();
	auto rigidbody = new RigidBodyComponent(false);
	rigidbody->SetKinematic(true);
	m_pBiteHitBox->AddComponent(rigidbody);

	std::shared_ptr<physx::PxGeometry> geom(new physx::PxBoxGeometry(1.f,1.f,1.f));
	auto collider = new ColliderComponent(geom, *pBouncyMaterial, physx::PxTransform(physx::PxQuat(XM_PI, physx::PxVec3(0, 0, 1))));
	collider->EnableTrigger(true);

	m_pBiteHitBox->AddComponent(collider);
	AddChild(m_pBiteHitBox);
	m_pBiteHitBox->GetTransform()->Displace(0,-5,0);
	m_pBiteHitBox->GetTransform()->Displace(ToXMVector(GetTransform()->GetForward())*20.f);
	
	m_pSensorHitBox = new GameObject();
	//auto child = new GameObject();
	rigidbody = new RigidBodyComponent(false);
	rigidbody->SetKinematic(true);
	m_pSensorHitBox->AddComponent(rigidbody);

	std::shared_ptr<physx::PxGeometry> geomSphere(new physx::PxSphereGeometry(20.0f));
	collider = new ColliderComponent(geomSphere, *pBouncyMaterial, physx::PxTransform(physx::PxQuat(XM_PI, physx::PxVec3(0, 0, 1))));
	collider->EnableTrigger(true);

	m_pSensorHitBox->AddComponent(collider);
	AddChild(m_pSensorHitBox);

	m_pBiteHitBox->SetOnTriggerCallBack([this](GameObject* trigger, GameObject* other, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(trigger);
		UNREFERENCED_PARAMETER(action);
		UNREFERENCED_PARAMETER(other);

		switch (action) 
		{ 
			case TriggerAction::ENTER: 
			if(other->GetTag() == L"Player")
			{
				m_Animation = Bite;
				m_pTarget = other;
				reinterpret_cast<MainCharacter*>(other)->TakeDamage(1);
			}break;
			case TriggerAction::LEAVE: break;
			default: break;
		}
	});

	m_pSensorHitBox->SetOnTriggerCallBack([this](GameObject* trigger, GameObject* other, GameObject::TriggerAction action)
	{
		UNREFERENCED_PARAMETER(trigger);
		UNREFERENCED_PARAMETER(action);
		UNREFERENCED_PARAMETER(other);

		switch (action) 
		{ 
			case TriggerAction::ENTER:
			if(other->GetTag() == L"Player")
			{
				Logger::Log(Info,L"Trigger");
				m_AIState = CHASING;
				m_pTarget = other;
			}
			break;

			case TriggerAction::LEAVE: 
			if(other == m_pTarget)
			{
				Logger::Log(Info,L"Out");
				m_pTarget = nullptr;
				m_AIState = PATROL;
			}
			break;
			default: break;
		}
	});

	GetTransform()->Scale(.5,.5,.5);
}

void EnemyBug::PostInitialize(const GameContext& gameContext)
{
	(gameContext);
	m_pModel->GetAnimator()->SetAnimation(Idle,true);
	m_pModel->GetAnimator()->Play();

	SetPatrolPoint(XMFLOAT3(GetTransform()->GetPosition().x,0,GetTransform()->GetPosition().z));
}

void EnemyBug::Update(const GameContext& gameContext)
{
	(gameContext);
	if(m_Animation != Die && m_Animation != Stagger)
	{
		auto target = HandleAIState();
		Move(target, gameContext);
	}
	else if(m_Animation == Die)
	{
		SetAnimation();
		if(m_pModel->GetAnimator()->IsDone()) SetDeleteFlag(true);
		return;
	}
	SetAnimation();

	if(!m_pModel->GetAnimator()->IsLooping())
		if(m_pModel->GetAnimator()->IsDone()) m_pModel->GetAnimator()->SetAnimation(Idle, true);

	auto currentPos = GetTransform()->GetPosition();
	m_pBiteHitBox->GetTransform()->Translate(ToXMVector(currentPos) + XMVector3Normalize(ToXMVector(GetTransform()->GetForward())*50.f));
	m_pSensorHitBox->GetTransform()->Translate(currentPos);
}

void EnemyBug::TakeDamage(int damage)
{
	if(m_Health > 0)
	{
		m_Animation = Stagger;
		m_Health -= damage;
		SoundManager::GetInstance()->GetSystem()->playSound(m_pHurt, 0, false, &m_pChannel);
		if(m_Health <= 0) 
		{
			m_Animation = Die;
			SoundManager::GetInstance()->GetSystem()->playSound(m_pDeath, 0, false, &m_pChannel);
		}
	}
	
}

void EnemyBug::SetPatrolPoint(XMFLOAT3 pos)
{
	m_PatrolCheckPoints.push_back(pos);
}

XMFLOAT3 EnemyBug::HandleAIState()
{
	switch (m_AIState)
	{
	case PATROL:
		{
			if(XMVector3Length(ToXMVector(m_PatrolCheckPoints[m_IdxSelected]) - ToXMVector(GetTransform()->GetWorldPosition())).m128_f32[0] < 20.0f )
			{
				m_IdxSelected++;
				if(m_IdxSelected >= m_PatrolCheckPoints.size()) m_IdxSelected = 0;
			}
			m_Animation = Walk;
			return m_PatrolCheckPoints[m_IdxSelected];
		}
	
	case CHASING: 
		{
			m_Animation = Run;
			return m_pTarget->GetTransform()->GetWorldPosition();
			break;
		}
	}
	return XMFLOAT3();
}

void EnemyBug::Move(XMFLOAT3 target, const GameContext& gameContext)
{
	(target);
	(gameContext);

	float velocity{};
	switch (m_AIState)
	{
	case PATROL:
		velocity = 10.f;
		break;
	case CHASING:
		velocity = 50.0f;
		break;
	}
	
	auto lerpPercent(.1f);

	XMFLOAT3 newVelocity = ToXMFloat3(XMVector3Normalize(ToXMVector(target) - ToXMVector(GetTransform()->GetPosition())) * velocity);

	m_Velocity.x = Lerp(m_Velocity.x,newVelocity.x,lerpPercent);
	m_Velocity.y = 0;
	m_Velocity.z = Lerp(m_Velocity.z,newVelocity.z,lerpPercent);	

	const auto controllerVel = XMFLOAT3(
		m_Velocity.x * gameContext.pGameTime->GetElapsed(),
		m_Velocity.y * gameContext.pGameTime->GetElapsed(),
		m_Velocity.z * gameContext.pGameTime->GetElapsed()
	);

	m_pController->Move(controllerVel);
	if(abs(m_Velocity.x) >= .1f || abs(m_Velocity.z) >= .1f)  GetTransform()->LookTowards(m_Velocity);
}

void EnemyBug::SetAnimation()
{
	if(m_pModel->GetAnimator()->GetClipIdx() != m_Animation)
	{
		switch (m_Animation)
		{
		case Stagger:
			m_pModel->GetAnimator()->SetAnimation(m_Animation,false);
			break;
		case Die:
			m_pModel->GetAnimator()->SetAnimation(m_Animation,false);
			break;
		default:
			m_pModel->GetAnimator()->SetAnimation(m_Animation,true);
			break;
		}
	}
		
}