#pragma once
#include "GameObject.h"

class ControllerComponent;
class CameraComponent;
class FollowCamera;
class ModelComponent;
class ParticleComponent;
class MainCharacterUI;

class MainCharacter final : public GameObject
{
public:
	enum CHARACTERSINPUTS : UINT
	{
		LEFT = 0,
		RIGHT,
		FORWARD,
		BACKWARD,
		JUMP,
		CROUCH,
		ATTACK,
		RESETCAM,
		SHOWHUD,
		DEBUG,
		DEBUG_IDLE,
		DEBUG_RUNNING,
		DEBUG_CROUCHING,
		DEBUG_CROUCHWALK,
		DEBUG_STARTJUMP,
		DEBUG_FALLING,
		DEBUG_LANDING,
		DEBUG_PUNCHING,
		DEBUG_WALLGRIP,
		DEBUG_WALLJUMP,
		DEBUG_WALKING,
		DEBUG_KICK,
		DEBUG_ADDSTAR,
		DEBUG_TAKEHIT,
		QUIT
	};

	enum Animation : UINT
	{
		Idle = 0,
		Running,
		Crouching,
		CrouchingWalk,
		StartJump,
		Falling,
		Landing,
		Punching,
		WallGripping,
		WallJump,
		Walking,
		Kick,
		Dive,
		LongJump,
		Hit,
		Died
	};


	MainCharacter(float radius = 2, float height = 5, float moveSpeed = 100);
	virtual ~MainCharacter() = default;

	MainCharacter(const MainCharacter& other) = delete;
	MainCharacter(MainCharacter&& other) noexcept = delete;
	MainCharacter& operator=(const MainCharacter& other) = delete;
	MainCharacter& operator=(MainCharacter&& other) noexcept = delete;

	void Initialize(const GameContext& gameContext) override;
	void PostInitialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;

	CameraComponent* GetCamera() const;
	void TakeDamage(int damage);
	int GetStarCollected() const { return m_StarsCollected; }
	int GetLivesRemaining() const {return m_Lives;}
	void CollectStar();
	void Reset();
	void SetAmountStarInLevel(int amount);
	bool IsDead() const {return m_IsDead;}
private:
	FollowCamera *m_pCamera;
	ControllerComponent *m_pController;
	ModelComponent *m_pModel;
	GameObject *m_pCameraTarget;
	MainCharacterUI *m_pUI; 

	GameObject *m_pParticleWallGrip,*m_pParticleRunning, 
	*m_pParticleHurt, *m_pParticleLand;

	GameObject *m_pPunchHitBox, *m_pJumpHitBox;

	Animation m_AnimationState;

	float m_Height,m_Radius;

	//Running
	float m_MaxRunVelocity, m_TerminalVelocity,	m_Gravity, 
		m_RunVelocity, m_JumpVelocity, m_MaxCrouchVelocity,
		m_WallJumpVelocity, m_LongJumpVelocity, m_DiveVelocity, m_KnockbackVelocity;

	float m_CameraTimer, m_CameraTimerMax,
		m_JumpTimer, m_JumpTimerMax,
		m_PunchCooldownTimer,m_PunchCooldownTimerMax;

	int m_NrJump, m_NrJumpMax,
	m_HealthMax, m_Health, 
	m_LivesMax, m_Lives,
	m_StarsCollected;

	bool m_HasKicked, m_IsDead;

	DirectX::XMFLOAT3 m_Velocity;
	DirectX::XMFLOAT3 m_MovementForward,  m_MovementRight;
	DirectX::XMFLOAT3 m_WallJumpDirection, m_LongJumpDirection, m_DiveDirection;
	
	FMOD::Sound* m_pHurt;
	FMOD::Sound* m_pLowHealth;
	FMOD::Channel* m_pChannel;


	static XMFLOAT2 GetMovement(const GameContext& gameContext);
	XMFLOAT3 HandleInput(const GameContext& gameContext, const XMFLOAT2& move);
	void SetAnimation() const;
	void HandleAnimationState(const GameContext& gameContext);
	void SetDebugAction(const GameContext& gameContext);
	void HandleCamera(const GameContext& gameContext, const XMFLOAT3& controllerVel);
	void HandleParticles() const;
	void InitParticles(const GameContext& gameContext);
	void InitHitBoxes();
	void MoveHitBoxes();
	void HandlePunchHb(GameObject* trigger, GameObject* other, TriggerAction action);
	void HandleJumpHb(GameObject* trigger, GameObject* other, TriggerAction action);
};





