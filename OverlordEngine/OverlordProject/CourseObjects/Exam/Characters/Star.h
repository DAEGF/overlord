#pragma once
#include "GameObject.h"
class Star :
	public GameObject
{
public:
	Star();
	virtual ~Star() = default;

	Star(const Star& other) = delete;
	Star(Star&& other) noexcept = delete;
	Star& operator=(const Star& other) = delete;
	Star& operator=(Star&& other) noexcept = delete;

	void Initialize(const GameContext& gameContext) override;
	void PostInitialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;

	void PickUp();
	bool IsPickedUp() const {return m_PickedUp; }
private:
	FMOD::Sound* m_pCollect;
	FMOD::Channel* m_pChannel;

	bool m_PickedUp;
	static bool m_LoadedMaterial;
	float m_TotalYaw;
	float m_DeathTimer, m_DeathTimerMax;
};

