#include "stdafx.h"
#include "MainCharacter.h"
#include "PhysxManager.h"
#include "Components.h"
#include "FollowCamera.h"
#include "GameScene.h"
#include "DirectXHelper.h"
#include "ModelAnimator.h"
#include "../OverlordProject/Materials/Shadow/SkinnedDiffuseMaterial_Shadow.h"
#include "OverlordGame.h"
#include "TimerHandler.h"
#include "MainCharacterUI.h"
#include "EnemyBug.h"
#include "../Scenes/PrototypeScene.h"
#include "SoundManager.h"


MainCharacter::MainCharacter(float radius, float height, float moveSpeed):
	m_pCamera(nullptr),
	m_pController(nullptr), m_pModel(nullptr),
	//Running
	m_pCameraTarget(nullptr), m_pUI(nullptr),
	m_pParticleWallGrip(nullptr), m_pParticleRunning(nullptr), m_pParticleHurt(nullptr), m_pParticleLand(nullptr),
	m_pPunchHitBox(nullptr), m_pJumpHitBox(nullptr),
	m_AnimationState(),
	m_Height(height),
	m_Radius(radius),
	m_MaxRunVelocity(moveSpeed), m_TerminalVelocity(50.f), m_Gravity(9.81f),
	m_RunVelocity(0), m_JumpVelocity(0), m_MaxCrouchVelocity(m_MaxRunVelocity / 2.f),
	m_WallJumpVelocity(m_MaxRunVelocity * 2.f), m_LongJumpVelocity(100.f), m_DiveVelocity(0),
	m_KnockbackVelocity(100.f),
	m_CameraTimer(0), m_CameraTimerMax(3.f), m_JumpTimer(0), m_JumpTimerMax(1.f), m_PunchCooldownTimer(0),
	m_PunchCooldownTimerMax(1),
	m_NrJump(0), m_NrJumpMax(3), m_HealthMax(4), m_Health(m_HealthMax), m_LivesMax(3), m_Lives(m_LivesMax),
	m_StarsCollected(0), m_HasKicked(false), m_IsDead(false),
	m_Velocity(0, 0, 0),
	m_MovementForward(), m_MovementRight(),
	m_WallJumpDirection(), m_LongJumpDirection(), m_DiveDirection(), m_pHurt(nullptr), m_pLowHealth(nullptr),
	m_pChannel(nullptr)
{
}

void MainCharacter::Initialize(const GameContext& gameContext)
{
	SetTag(L"Player");
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto pBouncyMaterial = physX->createMaterial(0, 0, 1);
	
	//TODO: Create controller
	m_pController = new ControllerComponent(pBouncyMaterial,m_Radius,m_Height);
	AddComponent(m_pController);
	
	auto rotatedModel  = new GameObject();
	m_pModel = new ModelComponent(L"Resources/Meshes/MainCharacter.ovm");
	rotatedModel->AddComponent(m_pModel);
	rotatedModel->GetTransform()->Rotate(0,180,0);
	rotatedModel->GetTransform()->Translate(0,-35,0);
	AddChild(rotatedModel);

	auto diffMat = new SkinnedDiffuseMaterial_Shadow();
	diffMat->SetDiffuseTexture(L"./Resources/Textures/MainChar_Diffuse.png");
	diffMat->SetLightDirection(gameContext.pShadowMapper->GetLightDirection());
	gameContext.pMaterialManager->AddMaterial(diffMat, PrototypeScene::Material::MainCharMat);
	m_pModel->SetMaterial(PrototypeScene::Material::MainCharMat);

	const auto height = 20.f;
	const auto distance = 60.f;

	auto parent = new GameObject();
	m_pCameraTarget = new GameObject();
	parent->AddChild(m_pCameraTarget);
	auto startPos = ToXMFloat3(-ToXMVector(GetTransform()->GetForward())*distance);
	m_pCameraTarget->GetTransform()->Translate(startPos);
	GetScene()->AddChild(parent);

	//TODO: Add a follow camera as child
	auto camera = new FollowCamera(10.f,distance,height,10.0f,3.0f,20.f);
	camera->AddTarget(this);
	camera->AddTargetPos(m_pCameraTarget);
	GetScene()->AddChild(camera);
	m_pCamera = camera;

	m_pUI = new MainCharacterUI();
	m_pUI->SetHealthMax(m_HealthMax);
	m_pUI->SetAmountStarsInLevel(10);
	m_pUI->SetLivesMax(m_LivesMax);
	GetScene()->AddChild(m_pUI);

	InitParticles(gameContext);

	InitHitBoxes();

	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->createSound("./Resources/Sounds/HurtSFX.wav", (FMOD_3D | FMOD_DEFAULT), NULL, &m_pHurt);
	fmodSystem->createSound("./Resources/Sounds/HeartbeatSFX.wav", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pLowHealth);

	GetTransform()->Scale(.1f,.1f,.1f);

	//TODO: Register all Input Actions
	gameContext.pInput->AddInputAction(InputAction(LEFT, Down, 'A'));
	gameContext.pInput->AddInputAction(InputAction(RIGHT, Down, 'D'));
	gameContext.pInput->AddInputAction(InputAction(FORWARD, Down, 'W'));
	gameContext.pInput->AddInputAction(InputAction(BACKWARD, Down, 'S'));
	gameContext.pInput->AddInputAction(InputAction(JUMP, Pressed, VK_SPACE,-1,XINPUT_GAMEPAD_A));
	gameContext.pInput->AddInputAction(InputAction(CROUCH, Down, VK_CONTROL,-1, XINPUT_GAMEPAD_LEFT_SHOULDER));
	gameContext.pInput->AddInputAction(InputAction(ATTACK,Pressed,'E',-1,XINPUT_GAMEPAD_B));
	gameContext.pInput->AddInputAction(InputAction(RESETCAM,Pressed,'Q',-1,XINPUT_GAMEPAD_RIGHT_SHOULDER));
	gameContext.pInput->AddInputAction(InputAction(SHOWHUD,Pressed,'R',-1,XINPUT_GAMEPAD_Y));

	gameContext.pInput->AddInputAction(InputAction(DEBUG, Down, VK_SHIFT));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_IDLE, Pressed, '0'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_CROUCHING, Pressed, '1'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_CROUCHWALK, Pressed, '2'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_FALLING, Pressed, '3'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_KICK, Pressed, '4'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_LANDING, Pressed, '5'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_PUNCHING, Pressed, '6'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_RUNNING, Pressed, '7'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_STARTJUMP, Pressed, '8'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_WALKING, Pressed, '9'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_WALLGRIP, Pressed, 'I'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_WALLJUMP, Pressed, 'O'));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_ADDSTAR, Pressed, VK_UP));
	gameContext.pInput->AddInputAction(InputAction(DEBUG_TAKEHIT, Pressed, VK_DOWN));
}

void MainCharacter::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//TODO: Set the camera as active
	//m_pCamera->GetTransform()->Translate(-10,20,0);
	m_pCamera->GetComponent<CameraComponent>()->SetActive();
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
	m_pModel->GetAnimator()->SetAnimation(0,true);
	m_pModel->GetAnimator()->Play();

	m_pJumpHitBox->SetOnTriggerCallBack([this](GameObject* trigger, GameObject* other, TriggerAction action)
	{
		HandleJumpHb(trigger, other, action);
	});

	m_pPunchHitBox->SetOnTriggerCallBack([this](GameObject* trigger, GameObject* other, TriggerAction action)
	{
		HandlePunchHb(trigger, other, action);
	});
}

void MainCharacter::Update(const GameContext& gameContext)
{
	using namespace DirectX;
	UNREFERENCED_PARAMETER(gameContext);
	
	if(m_AnimationState != Died)
	{
		XMFLOAT2 move = GetMovement(gameContext);
	
	//TODO: Update the character (Camera rotation, Character Movement, Character Gravity)
		auto controllerVel = HandleInput(gameContext, move);

		m_pController->Move(controllerVel);
	
		MoveHitBoxes();

		
		
		HandleCamera(gameContext,controllerVel);
		
		if(gameContext.pInput->IsActionTriggered(SHOWHUD)) m_pUI->ShowHud();
		if(m_PunchCooldownTimer != 0) UpdateAndCheckTimer(m_PunchCooldownTimer,m_PunchCooldownTimerMax,gameContext.pGameTime->GetElapsed());
	
	}
	
	if(gameContext.pInput->IsActionTriggered(DEBUG))
		{
			SetDebugAction(gameContext);
		}
	else HandleAnimationState(gameContext);
	SetAnimation();
	HandleParticles();

	//Logger::Log(Info,std::to_wstring(m_pPunchHitBox.y));
}

CameraComponent* MainCharacter::GetCamera() const
{
	return m_pCamera->GetComponent<CameraComponent>();
}

void MainCharacter::TakeDamage(int damage)
{
	SoundManager::GetInstance()->GetSystem()->playSound(m_pHurt, 0, false, &m_pChannel);
	m_Health -= damage;
	if(m_Health == 0)
	{
		m_Lives--;
		if(m_Lives == 0) 
		{
			m_AnimationState = Died;
		}
		else m_Health = m_HealthMax;
		m_pUI->SetLives(m_Lives);
	}
	else if (m_Health == 1) SoundManager::GetInstance()->GetSystem()->playSound(m_pLowHealth, 0, false, &m_pChannel);
	
	m_pUI->SetHealth(m_Health);
	m_pUI->ShowHud();
	if(m_AnimationState != Died) m_AnimationState = Hit;
	
}

XMFLOAT2 MainCharacter::GetMovement(const GameContext& gameContext)
{
	auto move = XMFLOAT2(0, 0);
	move.y = gameContext.pInput->IsActionTriggered(FORWARD) ? 1.0f : 0.0f;
	if (move.y == 0) move.y = -(gameContext.pInput->IsActionTriggered(BACKWARD) ? 1.0f : 0.0f);

	move.x = gameContext.pInput->IsActionTriggered(RIGHT) ? 1.0f : 0.0f;
	if (move.x == 0) move.x = -(gameContext.pInput->IsActionTriggered(LEFT) ? 1.0f : 0.0f);

	if(move.x == 0 && move.y == 0)
	{
		move = InputManager::GetThumbstickPosition();
	}

	return move;
}

XMFLOAT3 MainCharacter::HandleInput(const GameContext& gameContext,const XMFLOAT2& move)
{
	switch (m_AnimationState)
	{
		case Died:
		case Hit:
			return XMFLOAT3();
		default: 
		break;
	}

	XMFLOAT3 newVelocity{};
	float lerpPercent = .05f;

	if (move.x < .01f || move.y <= .01f)
	{
		m_MovementForward = XMFLOAT3(m_pCamera->GetTransform()->GetForward().x,0,m_pCamera->GetTransform()->GetForward().z);
		m_MovementRight = XMFLOAT3(m_pCamera->GetTransform()->GetRight().x,0,m_pCamera->GetTransform()->GetRight().z);
	}
	
	const auto direction = ToXMVector(m_MovementForward) * move.y + ToXMVector(m_MovementRight) * move.x;
	const auto tempVelocityY = m_Velocity.y;

	if (move.x != 0.f || move.y != 0.f)
	{
		if(gameContext.pInput->IsActionTriggered(CROUCH)) m_RunVelocity = m_MaxCrouchVelocity;
		else m_RunVelocity = m_MaxRunVelocity;
		XMStoreFloat3(&newVelocity, direction * m_RunVelocity);
	}
	else
	{
		m_RunVelocity = 0;
		XMStoreFloat3(&newVelocity, ToXMVector(XMFLOAT3(GetTransform()->GetForward().x,0,GetTransform()->GetForward().z)) * m_RunVelocity);
	}
	m_Velocity.y = tempVelocityY;

	if(m_pController->GetCollisionFlags() == physx::PxControllerCollisionFlag::eCOLLISION_SIDES)
	{
		if(m_AnimationState == WallJump)
		{
			XMStoreFloat3(&newVelocity,ToXMVector(m_WallJumpDirection) * m_WallJumpVelocity);
			lerpPercent = .9f;
		}

		if(m_Velocity.y <= 0)
		{
			const auto wallGripFriction = .1f;
			if(m_AnimationState != WallGripping) m_Velocity.y = 0;
			m_JumpVelocity -= (m_Gravity * wallGripFriction) * gameContext.pGameTime->GetElapsed();
			if (m_Velocity.y < -(m_TerminalVelocity * wallGripFriction)) 
			{
				m_Velocity.y = -(m_TerminalVelocity * wallGripFriction);
				m_JumpVelocity = 0;
			}
			if(gameContext.pInput->IsActionTriggered(JUMP))
			{
				XMStoreFloat3(&m_WallJumpDirection , -ToXMVector(GetTransform()->GetForward()));
 				XMStoreFloat3(&newVelocity, ToXMVector(m_WallJumpDirection) * m_WallJumpVelocity);
				m_Velocity.y = 100.f;
				lerpPercent = .9f;
			}
		}
	}
	else if (m_pController->GetCollisionFlags() != physx::PxControllerCollisionFlag::eCOLLISION_DOWN)
	{
		m_JumpVelocity -= m_Gravity * gameContext.pGameTime->GetElapsed();
		
		if(m_AnimationState == LongJump)
		{
			XMStoreFloat3(&newVelocity,ToXMVector(m_LongJumpDirection) * m_LongJumpVelocity);
			lerpPercent = .9f;
		}
		else if(m_AnimationState == WallJump)
		{
			XMStoreFloat3(&newVelocity,ToXMVector(m_WallJumpDirection) * m_WallJumpVelocity);
			lerpPercent = .9f;
		}
		else
		{
			if (m_Velocity.y < -m_TerminalVelocity) 
			{
				m_Velocity.y = -m_TerminalVelocity;
				m_JumpVelocity = 0;
			}

			if (gameContext.pInput->IsActionTriggered(ATTACK) && !m_HasKicked)	
			{
				m_JumpVelocity = 0.f; 
				m_Velocity.y = 50.f;
				m_HasKicked = true;
			}
		}
	}
	else if (gameContext.pInput->IsActionTriggered(JUMP))
	{
		if(gameContext.pInput->IsActionTriggered(CROUCH))
		{
			m_AnimationState = LongJump;
			XMStoreFloat3(&m_LongJumpDirection , ToXMVector(GetTransform()->GetForward()));
 			XMStoreFloat3(&newVelocity, ToXMVector(m_LongJumpDirection) * m_LongJumpVelocity);
			m_Velocity.y = 50.f;
			lerpPercent = .9f;
		}
		else
		{
			m_NrJump++;
			if(m_NrJump > m_NrJumpMax) m_NrJump = 1;
			m_Velocity.y = 100.f + (float(m_NrJump) * 25.f);
		}

	}
	else if (m_pController->GetCollisionFlags() == physx::PxControllerCollisionFlag::eCOLLISION_DOWN) 
	{
		if(m_Velocity.y < 1.0f) m_Velocity.y = -0.5f;
		m_JumpVelocity = 0;

		if(UpdateAndCheckTimer(m_JumpTimer,m_JumpTimerMax, gameContext.pGameTime->GetElapsed()))
			m_NrJump = 0;
		m_HasKicked = false;
	}

	m_Velocity.x = Lerp(m_Velocity.x,newVelocity.x,lerpPercent);
	m_Velocity.z = Lerp(m_Velocity.z,newVelocity.z,lerpPercent);

	m_Velocity.y += m_JumpVelocity;

	const auto controllerVel = XMFLOAT3(
		m_Velocity.x * gameContext.pGameTime->GetElapsed(),
		m_Velocity.y * gameContext.pGameTime->GetElapsed(),
		m_Velocity.z * gameContext.pGameTime->GetElapsed()
	);

	return controllerVel;
}

void MainCharacter::SetAnimation() const
{
	if(m_pModel->GetAnimator()->GetClipIdx() != m_AnimationState)
	{
		switch (m_AnimationState)
		{
		case StartJump:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case Landing:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case Punching:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case WallJump:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case Kick:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case Dive:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case Died:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case Hit:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			break;
		case LongJump:
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,false);
			//m_pModel->GetAnimator()->SetAnimationSpeed(.3f);
			break;
		default: 
			m_pModel->GetAnimator()->SetAnimation(m_AnimationState,true);
			break;
		}
	}
		
}

void MainCharacter::HandleAnimationState(const GameContext& gameContext)
{
	if(!m_pModel->GetAnimator()->IsLooping())
	{
		switch (m_AnimationState)
		{
		case StartJump:
			break;

		case Landing:
			break;

		case WallJump:
			if(m_pModel->GetAnimator()->IsDone() || m_pController->GetCollisionFlags() == physx::PxControllerCollisionFlag::eCOLLISION_DOWN) 
			{
				m_AnimationState = Idle;
				m_WallJumpDirection = XMFLOAT3(); 
			}
			return;

			case Died:
			if(m_pModel->GetAnimator()->IsDone()) 
				m_pModel->GetAnimator()->Pause();
				m_IsDead = true;
			break;

		case LongJump:
			if(m_pModel->GetAnimator()->IsDone() || m_pController->GetCollisionFlags() == physx::PxControllerCollisionFlag::eCOLLISION_DOWN)
			{
				m_AnimationState = Idle;
				m_LongJumpDirection = XMFLOAT3(); 
			}

		case Dive:
			if(m_pModel->GetAnimator()->IsDone())
			{
				m_AnimationState = Idle;
				m_LongJumpDirection = XMFLOAT3(); 
			}

		default:
			if(m_pModel->GetAnimator()->IsDone()) m_AnimationState = Idle;
			return;
		}	
	}

	if (abs(m_WallJumpDirection.x) >= .1f || abs(m_WallJumpDirection.z) >= .1f)
	{
		m_AnimationState = WallJump;
	}

	if (abs(m_LongJumpDirection.x) >= .1f || abs(m_LongJumpDirection.z) >= .1f)
	{
		m_AnimationState = LongJump;
	}

	if(m_pController->GetCollisionFlags() == physx::PxControllerCollisionFlag::eCOLLISION_SIDES)
	{
		if(m_AnimationState != WallJump)
		{
 			if(m_Velocity.y <= 0.f) m_AnimationState = WallGripping;
			if (abs(m_Velocity.x) >= .1f || abs(m_Velocity.z) >= .1f) 
				GetTransform()->LookTowards(XMFLOAT3(m_Velocity.x,0,m_Velocity.z),0.05f,true, true);
		}
	}
	else if(m_pController->GetCollisionFlags() != physx::PxControllerCollisionFlag::eCOLLISION_DOWN)
	{
		if(m_AnimationState != WallJump)
		{
			if(gameContext.pInput->IsActionTriggered(ATTACK))
			{
				m_AnimationState = Kick;
			}
			else
			{
				if(m_Velocity.y > 0) 
				{
					if(m_AnimationState != StartJump && m_AnimationState != Falling)
						m_AnimationState = StartJump;
				}
				else if(m_Velocity.y < -1.0f)
				{
					m_AnimationState = Falling;
				}

				if (abs(m_Velocity.x) >= .1f || abs(m_Velocity.z) >= .1f) 
					GetTransform()->LookTowards(XMFLOAT3(m_Velocity.x,0,m_Velocity.z),0.05f,true, true);	
			}
		}
	}
	else if (abs(m_Velocity.x) >= .1f || abs(m_Velocity.z) >= .1f) 
	{
		if(gameContext.pInput->IsActionTriggered(CROUCH)) m_AnimationState = CrouchingWalk;
		else m_AnimationState = Running;
		
		auto xz_vel = XMFLOAT2(m_Velocity.x,m_Velocity.z);
		const auto velocityVec = XMLoadFloat2(&xz_vel);
		const float length = XMVector2Length(velocityVec).m128_f32[0];
		if(length < m_MaxRunVelocity/2 && m_AnimationState != CrouchingWalk) m_AnimationState = Walking;
		
		if(m_AnimationState == CrouchingWalk || m_AnimationState == Walking)
		{
			auto speed = Clamp(length/m_MaxCrouchVelocity,0,1);
			m_pModel->GetAnimator()->SetAnimationSpeed(speed);
		}
		else 
		{
			auto speed = Clamp(length/m_MaxRunVelocity,0,1);
			m_pModel->GetAnimator()->SetAnimationSpeed(speed);
		}
		
		GetTransform()->LookTowards(XMFLOAT3(m_Velocity.x,0,m_Velocity.z),0.05f,true, true);
	}
	else 
	{
		if(m_AnimationState == Falling) m_AnimationState = Landing;
		else m_AnimationState = Idle;
		if(gameContext.pInput->IsActionTriggered(CROUCH)) m_AnimationState = Crouching;
		if(gameContext.pInput->IsActionTriggered(ATTACK)) m_AnimationState = Punching;
	}

	if(gameContext.pInput->IsActionTriggered(CROUCH) && gameContext.pInput->IsActionTriggered(JUMP)) m_AnimationState = LongJump;
}

void MainCharacter::SetDebugAction(const GameContext& gameContext)
{
	if(gameContext.pInput->IsActionTriggered(DEBUG_IDLE)) m_AnimationState = Dive;
	if(gameContext.pInput->IsActionTriggered(DEBUG_RUNNING)) m_AnimationState = Running;
	if(gameContext.pInput->IsActionTriggered(DEBUG_CROUCHING)) m_AnimationState = Crouching;
	if(gameContext.pInput->IsActionTriggered(DEBUG_CROUCHWALK)) m_AnimationState = CrouchingWalk;
	if(gameContext.pInput->IsActionTriggered(DEBUG_STARTJUMP)) m_AnimationState = StartJump;
	if(gameContext.pInput->IsActionTriggered(DEBUG_FALLING)) m_AnimationState = Falling;
	if(gameContext.pInput->IsActionTriggered(DEBUG_LANDING)) m_AnimationState = Landing;
	if(gameContext.pInput->IsActionTriggered(DEBUG_PUNCHING)) m_AnimationState = Punching;
	if(gameContext.pInput->IsActionTriggered(DEBUG_WALLGRIP)) m_AnimationState = WallGripping;
	if(gameContext.pInput->IsActionTriggered(DEBUG_WALLJUMP)) m_AnimationState = WallJump;
	if(gameContext.pInput->IsActionTriggered(DEBUG_WALKING)) m_AnimationState = Walking;
	if(gameContext.pInput->IsActionTriggered(DEBUG_KICK)) m_AnimationState = Kick;

	if(gameContext.pInput->IsActionTriggered(DEBUG_ADDSTAR))
	{
		if(m_StarsCollected < 10) m_StarsCollected++;
		m_pUI->SetAmountStarsFound(m_StarsCollected);
		m_pUI->ShowHud();
	}

	if(gameContext.pInput->IsActionTriggered(DEBUG_TAKEHIT)) TakeDamage(1);
}

void MainCharacter::HandleCamera(const GameContext& gameContext,const XMFLOAT3& controllerVel)
{
	m_pCamera->UpdateTargetVelocity(XMFLOAT3(controllerVel.x,0,controllerVel.z));

	m_pCameraTarget->GetParent()->GetTransform()->Translate(GetTransform()->GetWorldPosition());

	if(gameContext.pInput->IsActionTriggered(RESETCAM))
	{
		m_pCameraTarget->GetTransform()->Translate(ToXMFloat3(-ToXMVector(GetTransform()->GetForward())*m_pCamera->GetDistance()));
	}

	if (!(abs(m_Velocity.x) >= .1f || abs(m_Velocity.z) >= .1f))
	{
		if(UpdateAndCheckTimer(m_CameraTimer,m_CameraTimerMax,gameContext.pGameTime->GetElapsed()))
			m_pCameraTarget->GetTransform()->Translate(ToXMFloat3(-ToXMVector(GetTransform()->GetForward())*m_pCamera->GetDistance()));
	}
	else m_CameraTimer = 0.f;

	if(m_AnimationState == WallGripping)
	{
		m_pCameraTarget->GetTransform()->Translate(ToXMFloat3(-ToXMVector(GetTransform()->GetRight())*m_pCamera->GetDistance()));
	}
}

void MainCharacter::HandleParticles() const
{
	switch (m_AnimationState)
	{
		case WallGripping:
		m_pParticleWallGrip->SetActive(true);
		m_pParticleRunning->SetActive(false);
		break;
		case Running:
		m_pParticleWallGrip->SetActive(false);
		m_pParticleRunning->SetActive(true);
		break;
		case Landing:
		m_pParticleLand->SetActive(true);
		break;
		case Hit:
		m_pParticleHurt->SetActive(true);
		default:
		m_pParticleRunning->SetActive(false);
		m_pParticleWallGrip->SetActive(false);
	}
}

void MainCharacter::InitParticles(const GameContext& gameContext)
{
	(gameContext);

	m_pParticleWallGrip = new GameObject();
	auto particleEmitter = new ParticleEmitterComponent(L"./Resources/Textures/Smoke.png", 10);
	particleEmitter->SetVelocity(XMFLOAT3(0, 0.0f, 0));
	particleEmitter->SetMinSize(1.0f);
	particleEmitter->SetMaxSize(3.0f);
	particleEmitter->SetMinEnergy(1.0f);
	particleEmitter->SetMaxEnergy(2.0f);
	particleEmitter->SetMinSizeGrow(-6.5f);
	particleEmitter->SetMaxSizeGrow(-3.5f);
	particleEmitter->SetMinEmitterRange(0.2f);
	particleEmitter->SetMaxEmitterRange(0.5f);
	particleEmitter->SetColor(XMFLOAT4(1.f, 1.f, 1.f, 0.6f));
	particleEmitter->SetIsLooping(true);
	m_pParticleWallGrip->AddComponent(particleEmitter);
	AddChild(m_pParticleWallGrip);
	m_pParticleWallGrip->GetTransform()->Translate(0,m_Height,0);
	m_pParticleWallGrip->SetActive(false);

	m_pParticleLand = new GameObject();
	particleEmitter = new ParticleEmitterComponent(L"./Resources/Textures/Smoke.png", 10);
	particleEmitter->SetVelocity(XMFLOAT3(0, 5.0f, 0));
	particleEmitter->SetMinSize(1.0f);
	particleEmitter->SetMaxSize(3.0f);
	particleEmitter->SetMinEnergy(3.0f);
	particleEmitter->SetMaxEnergy(5.0f);
	particleEmitter->SetMinSizeGrow(-5.f);
	particleEmitter->SetMaxSizeGrow(-4.f);
	particleEmitter->SetMinEmitterRange(.1f);
	particleEmitter->SetMaxEmitterRange(.1f);
	particleEmitter->SetColor(XMFLOAT4(1.f, 1.f, 1.f, 0.6f));
	particleEmitter->SetIsLooping(false);
	particleEmitter->SetTimerMax(1.f);
	m_pParticleLand->AddComponent(particleEmitter);
	AddChild(m_pParticleLand);
	m_pParticleLand->SetActive(false);

	m_pParticleRunning = new GameObject();
	particleEmitter = new ParticleEmitterComponent(L"./Resources/Textures/Smoke.png", 15);
	particleEmitter->SetVelocity(XMFLOAT3(0, 1.0f, 0));
	particleEmitter->SetMinSize(1.0f);
	particleEmitter->SetMaxSize(2.0f);
	particleEmitter->SetMinEnergy(1.0f);
	particleEmitter->SetMaxEnergy(2.0f);
	particleEmitter->SetMinSizeGrow(-1.0f);
	particleEmitter->SetMaxSizeGrow(1.0f);
	particleEmitter->SetMinEmitterRange(0.1f);
	particleEmitter->SetMaxEmitterRange(0.1f);
	particleEmitter->SetColor(XMFLOAT4(1.f, 1.f, 1.f, 0.6f));
	particleEmitter->SetIsLooping(true);
	particleEmitter->SetTimerMax(.75f);
	m_pParticleRunning->AddComponent(particleEmitter);
	AddChild(m_pParticleRunning);
	m_pParticleRunning->GetTransform()->Displace(0,-5,0);
	m_pParticleRunning->SetActive(false);

	m_pParticleHurt = new GameObject();
	particleEmitter = new ParticleEmitterComponent(L"./Resources/Textures/StarParticle.png", 2);
	particleEmitter->SetVelocity(XMFLOAT3(0, 3.0f, 0));
	particleEmitter->SetMinSize(.2f);
	particleEmitter->SetMaxSize(1.0f);
	particleEmitter->SetMinEnergy(.5f);
	particleEmitter->SetMaxEnergy(2.f);
	particleEmitter->SetMinSizeGrow(-6.5f);
	particleEmitter->SetMaxSizeGrow(-3.5f);
	particleEmitter->SetMinEmitterRange(.1f);
	particleEmitter->SetMaxEmitterRange(.1f);
	particleEmitter->SetColor(XMFLOAT4(1.f, 1.f, 0.f, 1.f));
	particleEmitter->SetIsLooping(false);
	particleEmitter->SetTimerMax(1.f);
	m_pParticleHurt->AddComponent(particleEmitter);
	AddChild(m_pParticleHurt);
	m_pParticleHurt->GetTransform()->Translate(0,m_Height*2,0);
	m_pParticleHurt->SetActive(false);
	
}

void MainCharacter::InitHitBoxes()
{
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto bouncyMaterial = physX->createMaterial(0.5, 0.5, 1.0f);

	m_pJumpHitBox = new GameObject();
	//auto child = new GameObject();
	auto rigidbody = new RigidBodyComponent(false);
	rigidbody->SetKinematic(true);
	m_pJumpHitBox->AddComponent(rigidbody);

	std::shared_ptr<physx::PxGeometry> geom(new physx::PxBoxGeometry(1.f,1.f,1.f));
	auto collider = new ColliderComponent(geom, *bouncyMaterial, physx::PxTransform(physx::PxQuat(XM_PI, physx::PxVec3(0, 0, 1))));
	collider->EnableTrigger(true);
	
	m_pJumpHitBox->AddComponent(collider);
	//m_pJumpHitBox->AddChild(child);
	AddChild(m_pJumpHitBox);

	m_pPunchHitBox = new GameObject();
	//child = new GameObject();
	rigidbody = new RigidBodyComponent(false);
	rigidbody->SetKinematic(true);
	m_pPunchHitBox->AddComponent(rigidbody);

	std::shared_ptr<physx::PxGeometry> geomPunch(new physx::PxBoxGeometry(1.f,1.f,1.f));
	collider = new ColliderComponent(geomPunch, *bouncyMaterial, physx::PxTransform(physx::PxQuat(0, physx::PxVec3(0, 0, 1))));
	collider->EnableTrigger(true);

	m_pPunchHitBox->AddComponent(collider);
	m_pPunchHitBox->GetTransform()->Translate(0,10,10);
	//m_pPunchHitBox->AddChild(child);
	AddChild(m_pPunchHitBox);
}

void MainCharacter::CollectStar()
{
	if(m_StarsCollected < 10) m_StarsCollected++;
	m_pUI->SetAmountStarsFound(m_StarsCollected);
	m_pUI->ShowHud();
	Logger::Log(Info, L"StarHit");
}

void MainCharacter::Reset()
{
	GetTransform()->Translate(-20	,0,-20);
	m_Health = m_HealthMax;
	m_Lives = m_LivesMax;
	m_StarsCollected = 0;
	//m_Velocity = XMFLOAT3();
	m_pUI->SetHealth(m_Health);
	m_pUI->SetLives(m_Lives);
	m_pUI->SetAmountStarsFound(m_StarsCollected);
	//m_AnimationState = Idle;

}

void MainCharacter::SetAmountStarInLevel(int amount)
{
	m_pUI->SetAmountStarsInLevel(amount);
}

void MainCharacter::MoveHitBoxes()
{
	auto currentPos = GetTransform()->GetPosition();
	m_pPunchHitBox->GetTransform()->Translate(ToXMVector(currentPos) + XMVector3Normalize(ToXMVector(GetTransform()->GetForward())*50.f));
	m_pJumpHitBox->GetTransform()->Translate(currentPos.x,currentPos.y - 5.f,currentPos.z);
		
}

void MainCharacter::HandlePunchHb(GameObject* trigger, GameObject* other, TriggerAction action)
{
	(trigger);
	(action);
	Logger::Log(Warning,L"PunchTriggered");
	if(m_AnimationState == Punching && m_PunchCooldownTimer == 0 && other->GetTag() == L"Bug")
	{
		reinterpret_cast<EnemyBug*>(other)->TakeDamage(1);
		m_PunchCooldownTimer = .1f;
	}
}

void MainCharacter::HandleJumpHb(GameObject* trigger, GameObject* other, TriggerAction action)
{
	(trigger);
	(action);
	//if(!other) return;
	if(other->GetTag() == L"Bug" && action == TriggerAction::ENTER)
	{
		Logger::Log(Warning,L"BugHit");
		reinterpret_cast<EnemyBug*>(other)->TakeDamage(1);
		m_Velocity.y = 150.f;
	}
}

