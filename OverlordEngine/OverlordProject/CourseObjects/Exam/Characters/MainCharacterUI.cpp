#include "stdafx.h"
#include "MainCharacterUI.h"
#include "SpriteComponent.h"
#include "TransformComponent.h"
#include "OverlordGame.h"
#include "TimerHandler.h"


MainCharacterUI::MainCharacterUI():
	m_ShowingHud(false),
	m_HealthMax(0), m_Health(0),
	m_AmountStars(0),
	m_AmountStarsFound(0),
	m_Lives(0),
	m_HideHeight(-75.f),
	m_ShowHeight(75.f),
	m_HudShowTimer(0),
	m_HudShowTimerMax(5.f)
{
}

void MainCharacterUI::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto windowWidth = OverlordGame::GetGameSettings().Window.Width;

	//SET LOCATIONS
	auto healthPos = XMFLOAT2( windowWidth / 2.0f,  75.0f);
	auto starStartPos = XMFLOAT2(windowWidth - 200.f, -50.f);
	//starCollectibles
	for(int i{}; i < m_AmountStars; i++)
	{
		auto starRender = new GameObject();
		auto starRenderSprite = new SpriteComponent(L"./Resources/Textures/StarIcon.png",XMFLOAT2(.5f,.5f));
		starRender->AddComponent(starRenderSprite);
		AddChild(starRender);
		starRender->GetTransform()->Scale(0.31f, 0.31f, .31f);
		starRender->GetTransform()->Translate(starStartPos.x + float(15.f * i),starStartPos.y,.1f*float(i));
		m_StarIcons.push_back(starRender);
	}

	m_Health = m_HealthMax;
	m_Lives = m_LivesMax;


	for(int i{}; i < m_Lives; i++)
	{
		auto bg = new GameObject();
		auto bgSprite = new SpriteComponent(L"./Resources/Textures/HealthBgShadow.png",XMFLOAT2(.375f,.5f));
		bg->AddComponent(bgSprite);
		AddChild(bg);
		bg->GetTransform()->Scale(0.4f, 0.4f, .4f);
		bg->GetTransform()->Translate(healthPos.x - float(20.f * i),healthPos.y,.5f - (.1f*float(i)));
		m_pHealthBgs.push_back(bg);
	}
	//Health 
	
	for(int i{}; i < m_HealthMax; i++)
	{
		auto health = new GameObject();
		auto healthSprite = new SpriteComponent(L"./Resources/Textures/Health.png",XMFLOAT2(-.01f,1.01f));
		health->AddComponent(healthSprite);
		health->GetTransform()->Scale(0.25f, 0.25f, 1.f);
		health->GetTransform()->Translate(healthPos.x + 20.f,healthPos.y,.9f);
		health->GetTransform()->Rotate(0, 0, float(XM_PIDIV2*i),false);
		healthSprite->SetColor(XMFLOAT4(.0f,0.0f,1.f,1.f));
		AddChild(health);
		m_HealthIcons.push_back(health);
	}
}

void MainCharacterUI::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void MainCharacterUI::Update(const GameContext& gameContext)
{
	auto lerpPercent = .1f;
	auto scaleLerpPercent = .1f;
	UNREFERENCED_PARAMETER(gameContext);
	if(m_ShowingHud)
	{
		for(unsigned int i = 0; i < m_HealthIcons.size(); i++)
		{
			XMFLOAT3 originalPos = m_HealthIcons[i]->GetTransform()->GetPosition();
			
			m_HealthIcons[i]->GetTransform()->Translate(originalPos.x,Lerp(originalPos.y,m_ShowHeight,lerpPercent),originalPos.z);
			XMFLOAT3 originalScale = m_HealthIcons[i]->GetTransform()->GetScale();
			if(int(i) > m_Health - 1) m_HealthIcons[i]->GetTransform()->Scale(Lerp(originalScale.x,0,scaleLerpPercent),Lerp(originalScale.y,0,scaleLerpPercent),originalScale.z);
			else m_HealthIcons[i]->GetTransform()->Scale(Lerp(originalScale.x,.25f,scaleLerpPercent),Lerp(originalScale.y,.25f,scaleLerpPercent),originalScale.z);
		}

		for(int i = 0; i <= m_AmountStarsFound; i++)
		{
			int currentIdx = i-1;
			if(currentIdx >= 0) 
			{
				XMFLOAT3 originalPos = m_StarIcons[currentIdx]->GetTransform()->GetPosition();
				m_StarIcons[currentIdx]->GetTransform()->Translate(originalPos.x,Lerp(originalPos.y,m_ShowHeight,lerpPercent),originalPos.z);
			}
		}

		for(int i = 0; i < m_LivesMax; i++)
		{
			XMFLOAT3 originalPos = m_pHealthBgs[i]->GetTransform()->GetPosition();
			m_pHealthBgs[i]->GetTransform()->Translate(originalPos.x,Lerp(originalPos.y,m_ShowHeight,lerpPercent),originalPos.z);
			XMFLOAT3 originalScale = m_pHealthBgs[i]->GetTransform()->GetScale();
			if(int(i) > m_Lives - 1) m_pHealthBgs[i]->GetTransform()->Scale(Lerp(originalScale.x,0,scaleLerpPercent),Lerp(originalScale.y,0,scaleLerpPercent),originalScale.z);
			else m_pHealthBgs[i]->GetTransform()->Scale(Lerp(originalScale.x,.4f,scaleLerpPercent),Lerp(originalScale.y,.4f,scaleLerpPercent),originalScale.z);
		}
		if(UpdateAndCheckTimer(m_HudShowTimer,m_HudShowTimerMax,gameContext.pGameTime->GetElapsed())) m_ShowingHud = false;
	}
	else
	{
		for(auto pIcon : m_HealthIcons)
		{
			XMFLOAT3 originalPos = pIcon->GetTransform()->GetPosition();
			pIcon->GetTransform()->Translate(originalPos.x,Lerp(originalPos.y,m_HideHeight,lerpPercent),originalPos.z);
		}


		for(auto pIcon : m_StarIcons)
		{
			XMFLOAT3 originalPos = pIcon->GetTransform()->GetPosition();
			pIcon->GetTransform()->Translate(originalPos.x,Lerp(originalPos.y,m_HideHeight,lerpPercent),originalPos.z);
		}

		for(auto pIcon : m_pHealthBgs)
		{
			XMFLOAT3 originalPos = pIcon->GetTransform()->GetPosition();
			pIcon->GetTransform()->Translate(originalPos.x,Lerp(originalPos.y,m_HideHeight,lerpPercent),originalPos.z);
		}
	}
}

void MainCharacterUI::ShowHud()
{
	m_ShowingHud = true;
	m_HudShowTimer = 0;
}

