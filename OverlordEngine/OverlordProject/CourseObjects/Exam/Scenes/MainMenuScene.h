#pragma once
#include "GameScene.h"

class SpriteFont;

class MainMenuScene :
	public GameScene
{
public:
	enum class Menu
	{
		Start,
		Quit
	};

	MainMenuScene();
	virtual ~MainMenuScene() = default;

	MainMenuScene(const MainMenuScene& other) = delete;
	MainMenuScene(MainMenuScene&& other) noexcept = delete;
	MainMenuScene& operator=(const MainMenuScene& other) = delete;
	MainMenuScene& operator=(MainMenuScene&& other) noexcept = delete;

protected:
	void Initialize() override;
	void SceneActivated() override;
	void SceneDeactivated() override;
	void Update() override;
	void Draw() override;

private:
	SpriteFont * m_pFont;
	int m_idxSelected;
	std::vector<XMFLOAT2> m_MenuItemsPos;
	GameObject* m_pSelection;
	GameObject* m_pObj;

	FMOD::Sound* m_pMainSound;
	FMOD::Channel* m_pChannel;
};

