#include "stdafx.h"
#include "PrototypeScene.h"
#include "../Characters/MainCharacter.h"
#include "PhysxManager.h"
#include "PhysxProxy.h"
#include "Components.h"
#include "CubePrefab.h"
#include "../Characters/EnemyBug.h"
#include "OverlordGame.h"
#include "../Characters/Star.h"
#include "SceneManager.h"
#include "../../../Materials/SkyBoxMaterial.h"
#include "../../../Materials/Post/PostColorGrading.h"
#include "SoundManager.h"
#include "TimerHandler.h"
#include "../../../Materials/Shadow/DiffuseMaterial_Shadow.h"
#include "../../../Materials/Shadow/SkinnedDiffuseMaterial_Shadow.h"
#include "ContentManager.h"

PrototypeScene::PrototypeScene():
	GameScene(L"PrototypeScene"),
	m_pControlScreen(nullptr),
	m_IsPaused(false), m_IsShowingControlScreen(true), m_Won(false), m_Lost(false),
	m_idxSelected(0), m_ControlScreenTimer(0), m_ControlScreenTimerMax(5), m_EndTimer(0), m_EndTimerMax(5),
	m_pPlayer(nullptr),
	m_StarsToCollect(5), m_AmountEnemies(3),
	m_pMainTheme(nullptr), m_pEndTheme(nullptr), m_pChannel(nullptr)
{
}

void PrototypeScene::Win()
{
	m_Won = true;
	m_pWinScreen->SetActive(true);
	if(UpdateAndCheckTimer(m_EndTimer,m_EndTimerMax,GetGameContext().pGameTime->GetElapsed()))
	{
		m_pChannel->stop();
		SceneManager::GetInstance()->SetActiveGameScene(L"MainMenu");
	}
}

void PrototypeScene::Lose()
{
	m_Lost = true;
	if (m_pPlayer->IsDead())
	{
		m_pLoseScreen->SetActive(true);
		if(UpdateAndCheckTimer(m_EndTimer,m_EndTimerMax,GetGameContext().pGameTime->GetElapsed()))
		{
			m_pChannel->stop();
			SceneManager::GetInstance()->SetActiveGameScene(L"MainMenu");
		}
	}
}

void PrototypeScene::Initialize()
{
	GetPhysxProxy()->EnablePhysxDebugRendering(false);
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto bouncyMaterial = physX->createMaterial(0.5, 0.5, 1.0f);
	GetGameContext().pShadowMapper->SetLight(
		{ -95.6139526f,66.1346436f,-41.1850471f },
		{ 0.740129888f, -0.597205281f, 0.309117377f });

	auto starMat = new DiffuseMaterial_Shadow();
	starMat->SetDiffuseTexture(L"./Resources/Textures/Star_Diffuse.png");
	starMat->SetLightDirection(GetGameContext().pShadowMapper->GetLightDirection());
	GetGameContext().pMaterialManager->AddMaterial(starMat, PrototypeScene::StarMat);

	auto diffMat = new SkinnedDiffuseMaterial_Shadow();
	diffMat->SetDiffuseTexture(L"./Resources/Textures/Insect_Diffuse.png");
	diffMat->SetLightDirection(GetGameContext().pShadowMapper->GetLightDirection());
	GetGameContext().pMaterialManager->AddMaterial(diffMat, PrototypeScene::BugMat);

	auto geomMat = new DiffuseMaterial_Shadow();
	geomMat->SetDiffuseTexture(L"./Resources/Textures/Level_Diffuse.png");
	geomMat->SetLightDirection(GetGameContext().pShadowMapper->GetLightDirection());
	GetGameContext().pMaterialManager->AddMaterial(geomMat, LevelMat);

	//GROUND 
	auto ground = new GameObject();
	ground->AddComponent(new RigidBodyComponent(true));

	std::shared_ptr<physx::PxGeometry> geom(new physx::PxPlaneGeometry());
	ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, 
		physx::PxTransform(physx::PxQuat(DirectX::XM_PIDIV2, physx::PxVec3(0, 0, 1)))));
	AddChild(ground);

	InitGeometry();

	InitObjects();

	auto skyObj = new GameObject();
	auto m_pBox = new ModelComponent(L"Resources/Meshes/Box.ovm");
	auto skyBox = new SkyBoxMaterial();
	skyBox->SetSkyBoxTexture(L"./Resources/Textures/SkyBox.dds");
	GetGameContext().pMaterialManager->AddMaterial(skyBox,SkyBoxMat);
	m_pBox->SetMaterial(SkyBoxMat);

	skyObj->AddComponent(m_pBox);
	AddChild(skyObj);

	InitPauseMenuElements();

	auto colorGradPP = new PostColorGrading();
	colorGradPP->SetLookUpMapTexture(L"./Resources/Textures/LookUpTableGraded.png");
	AddPostProcessingEffect(colorGradPP);

	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->createSound("./Resources/Sounds/MainTheme.mp3", (FMOD_2D | FMOD_LOOP_NORMAL), NULL, &m_pMainTheme);
	fmodSystem->createSound("./Resources/Sounds/EndTheme.mp3", (FMOD_2D | FMOD_DEFAULT), NULL, &m_pEndTheme);


	GetGameContext().pInput->AddInputAction(InputAction(PAUSE, Pressed, VK_ESCAPE,-1, XINPUT_GAMEPAD_START));
	GetGameContext().pInput->AddInputAction(InputAction(UP, Pressed, 'W', -1, XINPUT_GAMEPAD_DPAD_UP));
	GetGameContext().pInput->AddInputAction(InputAction(DOWN, Pressed, 'S', -1, XINPUT_GAMEPAD_DPAD_DOWN));
	GetGameContext().pInput->AddInputAction(InputAction(SELECT, Pressed, VK_SPACE, -1, XINPUT_GAMEPAD_A));
}

void PrototypeScene::Update()
{
	if(m_Won || m_pPlayer->GetStarCollected() == m_StarsToCollect) 
	{
		if(!m_Won)
		{
			auto fmodSystem = SoundManager::GetInstance()->GetSystem();
			fmodSystem->playSound(m_pEndTheme, 0, false, &m_pChannel);
		}
		Win();
		return;
	}
	if(m_Lost || m_pPlayer->GetLivesRemaining() <= 0)
	{
		Lose();
		return;
	}

	if(m_IsShowingControlScreen)
	{
		HandleControlScreen();
		return;
	}
	CheckAndHandlePause();
}

void PrototypeScene::Draw()
{
}

void PrototypeScene::SceneActivated()
{
	Reset();
}

void PrototypeScene::SceneDeactivated()
{
	m_pChannel->stop();
}

void PrototypeScene::InitPauseMenuElements()
{	
	auto windowWidth = OverlordGame::GetGameSettings().Window.Width;
	auto windowHeight = OverlordGame::GetGameSettings().Window.Height;

	m_PauseMenuPos.push_back(XMFLOAT2(windowWidth / 2.0f - 175.f, windowHeight / 2.0f  - 50.0f));
	m_PauseMenuPos.push_back(XMFLOAT2(windowWidth / 2.0f - 175.0f, windowHeight / 2.0f + 20.0f));
	m_PauseMenuPos.push_back(XMFLOAT2(windowWidth / 2.0f - 175.0f, windowHeight / 2.0f + 90.0f));


	auto bg = new GameObject();
	auto bgSprite = new SpriteComponent(L"./Resources/Textures/PauseMenu.png");
	auto bgDimensions = bgSprite->GetDimensions();
	bg->AddComponent(bgSprite);
	bg->GetTransform()->Scale(windowWidth / bgDimensions.x, windowHeight / bgDimensions.y, .1f);
	bg->GetTransform()->Translate(0, 0, .3f);
	AddChild(bg);
	bg->SetActive(false);
	bgSprite->SetColor(XMFLOAT4(1.f,1.f,1.f,1.f));
	m_PauseMenuElem.push_back(bg);

	m_pControlScreen = new GameObject();
	auto screenSprite = new SpriteComponent(L"./Resources/Textures/ControlScreen.png");
	auto spriteDimensions = screenSprite->GetDimensions();
	m_pControlScreen->AddComponent(screenSprite);
	m_pControlScreen->GetTransform()->Scale(windowWidth / spriteDimensions.x, windowHeight / spriteDimensions.y, 1.f);
	m_pControlScreen->GetTransform()->Translate(0, 0, 0.3f);
	AddChild(m_pControlScreen);
	m_pControlScreen->SetActive(false);
	screenSprite->SetColor(XMFLOAT4(1.f,1.f,1.f,1.f));

	m_pWinScreen = new GameObject();
	screenSprite = new SpriteComponent(L"./Resources/Textures/WinScreen.png");
	spriteDimensions = screenSprite->GetDimensions();
	m_pWinScreen->AddComponent(screenSprite);
	m_pWinScreen->GetTransform()->Scale(windowWidth / spriteDimensions.x, windowHeight / spriteDimensions.y, 1.f);
	m_pWinScreen->GetTransform()->Translate(0, 0, .3f);
	AddChild(m_pWinScreen);
	m_pWinScreen->SetActive(false);
	screenSprite->SetColor(XMFLOAT4(1.f,1.f,1.f,1.f));

	m_pLoseScreen = new GameObject();
	screenSprite = new SpriteComponent(L"./Resources/Textures/LoseScreen.png");
	spriteDimensions = screenSprite->GetDimensions();
	m_pLoseScreen->AddComponent(screenSprite);
	m_pLoseScreen->GetTransform()->Scale(windowWidth / spriteDimensions.x, windowHeight / spriteDimensions.y, 1.f);
	m_pLoseScreen->GetTransform()->Translate(0, 0, .2f);
	AddChild(m_pLoseScreen);
	m_pLoseScreen->SetActive(false);
	screenSprite->SetColor(XMFLOAT4(1.f,1.f,1.f,1.f));

	auto scale = 0.5f;
	
	//text menu items
	auto mainMenu = new GameObject();
	auto mainMenuSprite = new SpriteComponent(L"./Resources/Textures/MainMenu.png");
	mainMenu->AddComponent(mainMenuSprite);
	mainMenu->GetTransform()->Translate(m_PauseMenuPos[0].x, m_PauseMenuPos[0].y, 0.2f);
	mainMenu->GetTransform()->Scale(scale, scale, scale);
	AddChild(mainMenu);
	mainMenu->SetActive(false);
	m_PauseMenuElem.push_back(mainMenu);

	auto restart = new GameObject();
	auto restartSprite = new SpriteComponent(L"./Resources/Textures/Restart.png");
	restart->AddComponent(restartSprite);
	restart->GetTransform()->Translate(m_PauseMenuPos[1].x, m_PauseMenuPos[1].y, 0.2f);
	restart->GetTransform()->Scale(scale, scale, scale);
	AddChild(restart);
	restart->SetActive(false);
	m_PauseMenuElem.push_back(restart);

	auto quit = new GameObject();
	auto quitSprite = new SpriteComponent(L"./Resources/Textures/Quit.png");
	quit->AddComponent(quitSprite);
	quit->GetTransform()->Translate(m_PauseMenuPos[2].x, m_PauseMenuPos[2].y, 0.2f);
	quit->GetTransform()->Scale(scale, scale, scale);
	AddChild(quit);
	quit->SetActive(false);
	m_PauseMenuElem.push_back(quit);

	//Selector
	auto selector = new GameObject();
	auto selectorSprite = new SpriteComponent(L"./Resources/Textures/HeadHud.png");
	selector->AddComponent(selectorSprite);
	selector->GetTransform()->Scale(0.1f, 0.1f, 0.1f);
	selector->GetTransform()->Translate(m_PauseMenuPos[0].x, m_PauseMenuPos[0].y, 0.2f);
	AddChild(selector);
	selector->SetActive(false);
	m_PauseMenuElem.push_back(selector);
}

void PrototypeScene::Reset()
{
	m_Won = false;
	m_Lost = false;
	m_IsShowingControlScreen = true;

	GetGameContext().pGameTime->Start();
	m_IsPaused = false;
	for (GameObject* curr : m_PauseMenuElem)
	{
		curr->SetActive(false);
	}
	m_pLoseScreen->SetActive(false);
	m_pWinScreen->SetActive(false);

	for(int i = 0; i < m_AmountEnemies; i++ )
	{
		if(!m_pEnemies[i])
		{
			m_pEnemies[i] = new EnemyBug();
			AddChild(m_pEnemies[i]);
		}
		m_pEnemies[i]->GetTransform()->GetTransform()->Translate(m_EnemyPosition[i]);
		m_pEnemies[i]->SetPatrolPoint(XMFLOAT3(randF(m_EnemyPosition[i].x - 50.f,m_EnemyPosition[i].x + 50.f), m_EnemyPosition[i].y,
		randF(m_EnemyPosition[i].z - 50.f,m_EnemyPosition[i].z + 50.f)));
	}

	for(int i = 0; i < m_StarsToCollect; i++)
	{
		if(!m_pStars[i] || m_pStars[i]->IsPickedUp())
		{
			if(m_pStars[i]) m_pStars[i]->SetDeleteFlag(true);
			m_pStars[i] = new Star();
			m_pStars[i]->GetTransform()->Translate(m_StarsPositions[i]);
			AddChild(m_pStars[i]);
		}
	}

	
	m_pPlayer->Reset();
	m_pPlayer->SetAmountStarInLevel(m_StarsToCollect);

	m_pChannel->stop();
	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->playSound(m_pMainTheme, 0, false, &m_pChannel);
}

void PrototypeScene::CheckAndHandlePause()
{
	if (GetGameContext().pInput->IsActionTriggered(PAUSE))
	{
		if (!m_IsPaused && GetGameContext().pGameTime->IsRunning())
		{
				GetGameContext().pGameTime->Stop();
				m_IsPaused = true;
				for (GameObject* curr : m_PauseMenuElem)
				{
					curr->SetActive(true);
				}
				m_idxSelected = 0;
		}
		else if (m_IsPaused)
		{
			GetGameContext().pGameTime->Start();
			m_IsPaused = false;
			for (GameObject* curr : m_PauseMenuElem)
			{
				curr->SetActive(false);
			}
		}
	}
	if (!m_IsPaused && !GetGameContext().pGameTime->IsRunning()) GetGameContext().pGameTime->Start();

	
	if (m_IsPaused)
	{
		if (GetGameContext().pInput->IsActionTriggered(UP))
		{
			if (m_idxSelected > 0) m_idxSelected--;
		}
		if (GetGameContext().pInput->IsActionTriggered(DOWN))
		{
			if (m_idxSelected < 2) m_idxSelected++;
		}
		if (GetGameContext().pInput->IsActionTriggered(SELECT))
		{
			switch (m_idxSelected)
			{
				case 0:
					SceneManager::GetInstance()->SetActiveGameScene(L"MainMenu");
					Reset();
					//m_pChannel->stop();
					m_IsPaused = false;
					break;
				case 1:
					Reset();
					m_IsPaused = false;
					break;
				case 2:
					OverlordGame::Quit();
					break;
				default:
					break;
			}
		}
		m_PauseMenuElem.back()->GetTransform()->Translate(m_PauseMenuPos[m_idxSelected].x - 20.0f , Lerp(m_PauseMenuElem.back()->GetTransform()->GetPosition().y,m_PauseMenuPos[m_idxSelected].y,.1f), 0.1f);
		if (!m_IsPaused && !GetGameContext().pGameTime->IsRunning()) GetGameContext().pGameTime->Start();
	}
}

void PrototypeScene::HandleControlScreen()
{
	if(UpdateAndCheckTimer(m_ControlScreenTimer,m_ControlScreenTimerMax,GetGameContext().pGameTime->GetElapsed())) 	m_IsShowingControlScreen = false;
	m_pControlScreen->SetActive(m_IsShowingControlScreen);
}

void PrototypeScene::InitGeometry()
{
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto bouncyMaterial = physX->createMaterial(0.5, 0.5, 1.0f);

	auto tower = new GameObject();
	auto modelComp = new ModelComponent(L"Resources/Meshes/Level/Tower.ovm");
	modelComp->SetMaterial(LevelMat);
	tower->AddComponent(modelComp);
	auto pRigidBody = new RigidBodyComponent(false);
	pRigidBody->SetKinematic(true);
	tower->AddComponent(pRigidBody);
	std::shared_ptr<physx::PxGeometry> convexMesh{new physx::PxConvexMeshGeometry(ContentManager::Load<physx::PxConvexMesh>(L"Resources/Meshes/Level/Tower.ovpc"),physx::PxMeshScale(5.0f)) };
	auto collider = new ColliderComponent(convexMesh,*bouncyMaterial);
	tower->AddComponent(collider);
	tower->GetTransform()->Rotate(90,0,0);
	AddChild(tower);
	tower->GetTransform()->Scale(5.0f,5.0f,5.0f);
	tower->GetTransform()->Translate(-20.f,0.f,-20.f);

	auto field = new GameObject();
	modelComp = new ModelComponent(L"Resources/Meshes/Level/Field.ovm");
	modelComp->SetMaterial(LevelMat);
	field->AddComponent(modelComp);
	pRigidBody = new RigidBodyComponent(false);
	pRigidBody->SetKinematic(true);
	field->AddComponent(pRigidBody);
	std::shared_ptr<physx::PxGeometry> convexMesh2{new physx::PxConvexMeshGeometry(ContentManager::Load<physx::PxConvexMesh>(L"Resources/Meshes/Level/Field.ovpc"),physx::PxMeshScale(25.0f)) };
	collider = new ColliderComponent(convexMesh2,*bouncyMaterial);
	field->AddComponent(collider);
	field->GetTransform()->Rotate(90,0,0);
	AddChild(field);
	field->GetTransform()->Scale(25.0f,25.0f,25.0f);
	field->GetTransform()->Translate(-0.f,-0.f,-0.f);
}

void PrototypeScene::InitObjects()
{
	m_EnemyPosition.push_back(XMFLOAT3(75,5,75));
	m_EnemyPosition.push_back(XMFLOAT3(0,5,50));
	m_EnemyPosition.push_back(XMFLOAT3(50,5,50));

	if(m_EnemyPosition.size() < unsigned int(m_AmountEnemies)) Logger::LogWarning(L"Not loading enough enemies");

	m_StarsPositions.push_back(XMFLOAT3(50,20,50));
	m_StarsPositions.push_back(XMFLOAT3(75,15,0));
	m_StarsPositions.push_back(XMFLOAT3(-50,15,-50));
	m_StarsPositions.push_back(XMFLOAT3(0,15,-75));
	m_StarsPositions.push_back(XMFLOAT3(20,30,20));

	if(m_StarsPositions.size() < unsigned int(m_StarsToCollect)) Logger::LogWarning(L"Not loading enough stars");

	m_pPlayer = new MainCharacter(3,5,50.f);
	m_pPlayer->GetTransform()->Translate(-20,0,-20);
	AddChild(m_pPlayer);

	for(int i{}; i < m_AmountEnemies; i++)
	{
		m_pEnemies.push_back(new EnemyBug());
		m_pEnemies[i]->GetTransform()->Translate(m_EnemyPosition[i]);
		m_pEnemies[i]->SetPatrolPoint(XMFLOAT3(randF(m_EnemyPosition[i].x - 50.f,m_EnemyPosition[i].x + 50.f), m_EnemyPosition[i].y,
		randF(m_EnemyPosition[i].z - 50.f,m_EnemyPosition[i].z + 50.f)));
		AddChild(m_pEnemies[i]);
	}

	for(int i = 0; i < m_StarsToCollect; i++)
	{
		m_pStars.push_back(new Star());
		m_pStars[i]->GetTransform()->Translate(m_StarsPositions[i]);
		AddChild(m_pStars[i]);
	}

}

