#pragma once
#include "GameScene.h"
#include "../Characters/EnemyBug.h"

class MainCharacter;
class Star;

class PrototypeScene final : public GameScene
{
public:
	enum Input : UINT
	{
		PAUSE = 50,
		UP,
		DOWN,
		SELECT
	};

	enum Options : UINT
	{
		MainMenu = 0,
		Restart,
		Quit
	};

	enum Material : UINT
	{
		MainCharMat = 0,
		BugMat,
		StarMat,
		LevelMat,
		SkyBoxMat
	};

	PrototypeScene();
	virtual ~PrototypeScene() = default;

	PrototypeScene(const PrototypeScene& other) = delete;
	PrototypeScene(PrototypeScene&& other) noexcept = delete;
	PrototypeScene& operator=(const PrototypeScene& other) = delete;
	PrototypeScene& operator=(PrototypeScene&& other) noexcept = delete;

	void Win();
	void Lose();
protected:
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void SceneActivated() override;
	void SceneDeactivated() override;

private:
	void InitPauseMenuElements();
	void Reset();
	void CheckAndHandlePause();
	void HandleControlScreen();
	void InitGeometry();
	void InitObjects();
	
	GameObject* m_pControlScreen;
	GameObject *m_pWinScreen, *m_pLoseScreen;
	std::vector<GameObject*> m_PauseMenuElem;
	std::vector<XMFLOAT2> m_PauseMenuPos;
	bool m_IsPaused, m_IsShowingControlScreen,
		m_Won, m_Lost;
	int m_idxSelected;

	float m_ControlScreenTimer, m_ControlScreenTimerMax,
		m_EndTimer, m_EndTimerMax;

	MainCharacter* m_pPlayer;

	int m_StarsToCollect;
	int m_AmountEnemies;
	std::vector<Star*> m_pStars;
	std::vector<EnemyBug*> m_pEnemies;

	std::vector<XMFLOAT3> m_EnemyPosition;
	std::vector<XMFLOAT3> m_StarsPositions;
	std::vector<XMFLOAT3> m_GeometryPosition;

	FMOD::Sound* m_pMainTheme;
	FMOD::Sound* m_pEndTheme;
	FMOD::Channel* m_pChannel;
};

