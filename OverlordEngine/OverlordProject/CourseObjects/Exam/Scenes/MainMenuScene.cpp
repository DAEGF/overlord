#include "stdafx.h"
#include "MainMenuScene.h"
#include "OverlordGame.h"
#include "FixedCamera.h"
#include "SpriteComponent.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "ContentManager.h"
#include "TransformComponent.h"
#include "SoundManager.h"

MainMenuScene::MainMenuScene():
	GameScene(L"MainMenu"),
	m_pFont(nullptr),
	m_pSelection(nullptr),
	m_MenuItemsPos{},
	m_idxSelected(0)
{
}

void MainMenuScene::Initialize()
{
	//WINDOW SIZES
	auto windowWidth = OverlordGame::GetGameSettings().Window.Width;
	auto windowHeight = OverlordGame::GetGameSettings().Window.Height;

	//SET LOCATIONS
	m_MenuItemsPos.push_back(XMFLOAT2( windowWidth / 2.0f - 125.0f, windowHeight / 2.0f + 35.0f));
	m_MenuItemsPos.push_back(XMFLOAT2(windowWidth / 2.0f - 125.0f, windowHeight / 2.0f + 175.0f));


	//CAMERA
	auto camera = new FixedCamera();
	AddChild(camera);
	SetActiveCamera(camera->GetComponent<CameraComponent>());

    camera->GetTransform()->Translate(0.0f, 200.0f, 0.0f);
	camera->GetTransform()->Rotate(90.0f, 0.0f, 0.0f);

	
	//BG
	auto bg = new GameObject();
	auto bgSprite = new SpriteComponent(L"./Resources/Textures/MenuBg.png");
	bg->AddComponent(bgSprite);
	AddChild(bg);
	
	auto bgDimensions = bgSprite->GetDimensions();
	bg->GetTransform()->Scale(windowWidth / bgDimensions.x, windowHeight / bgDimensions.y, 1.0f);
	bg->GetTransform()->Translate(0,0,.2f);

	//text menu items
	auto start = new GameObject();
	auto startSprite = new SpriteComponent(L"./Resources/Textures/Start.png");
	start->AddComponent(startSprite);
	AddChild(start);
	start->GetTransform()->Rotate(1,0,0);
	start->GetTransform()->Translate(m_MenuItemsPos[0].x, m_MenuItemsPos[0].y, 0.5f);

	auto quit = new GameObject();
	auto quitSprite = new SpriteComponent(L"./Resources/Textures/Quit.png");
	quit->AddComponent(quitSprite);
	AddChild(quit);
	quit->GetTransform()->Translate(m_MenuItemsPos[1].x, m_MenuItemsPos[1].y, 0.5f);

	//SELECTION
	m_pSelection = new GameObject();
	auto selectionSprite = new SpriteComponent(L"./Resources/Textures/HeadHud.png", XMFLOAT2(.5f,.5f));
	m_pSelection->AddComponent(selectionSprite);
	AddChild(m_pSelection);
	m_pSelection->GetTransform()->Scale(0.2f, 0.2f, 0.2f);
	m_pSelection->GetTransform()->Translate(m_MenuItemsPos[0].x, m_MenuItemsPos[0].y, 0.5f);

	//SOUND
	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->createSound("./Resources/Sounds/MenuTheme.mp3", FMOD_2D | FMOD_LOOP_NORMAL, NULL, &m_pMainSound);

	//INPUT
	GetGameContext().pInput->AddInputAction(InputAction(90, Pressed, 'W',-1,XINPUT_GAMEPAD_DPAD_UP));
	GetGameContext().pInput->AddInputAction(InputAction(91, Pressed, 'S',-1, XINPUT_GAMEPAD_DPAD_DOWN));
	GetGameContext().pInput->AddInputAction(InputAction(92, Pressed, VK_SPACE, -1, XINPUT_GAMEPAD_A));
}

void MainMenuScene::SceneActivated()
{
	auto fmodSystem = SoundManager::GetInstance()->GetSystem();
	fmodSystem->playSound(m_pMainSound, 0, false, &m_pChannel);
}

void MainMenuScene::SceneDeactivated()
{
	m_pChannel->stop();
}

void MainMenuScene::Update()
{
	if(GetGameContext().pInput->IsActionTriggered(90))
	{
		if (m_idxSelected > 0) m_idxSelected--;
	}
	if (GetGameContext().pInput->IsActionTriggered(91))
	{
		if (m_idxSelected < 1) m_idxSelected++;
	}
	if (GetGameContext().pInput->IsActionTriggered(92))
	{
		switch (m_idxSelected) 
		{
		case 0:
			SceneManager::GetInstance()->SetActiveGameScene(L"PrototypeScene");
			break;
		case 1:
			OverlordGame::Quit();
			break;
		default: 
			break;
		}
	}

	m_pSelection->GetTransform()->Translate(m_MenuItemsPos[m_idxSelected].x - 50.f, Lerp(m_pSelection->GetTransform()->GetPosition().y,m_MenuItemsPos[m_idxSelected].y + 50.f,.1f) , 0.2f);
}

void MainMenuScene::Draw()
{
}