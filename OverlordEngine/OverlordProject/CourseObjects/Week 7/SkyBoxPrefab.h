#pragma once
#include "GameObject.h"


class ModelComponent;

class SkyBoxPrefab : public GameObject
{
public:
	SkyBoxPrefab();
	~SkyBoxPrefab();

protected:
	virtual void Initialize(const GameContext& gameContext) override;

private:
	ModelComponent * m_pBox = nullptr;

private:
	SkyBoxPrefab(const SkyBoxPrefab& obj) = delete;
	SkyBoxPrefab operator=(const SkyBoxPrefab& obj) = delete;
};

