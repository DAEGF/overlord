#include "stdafx.h"
#include "SkyBoxPrefab.h"
#include "../OverlordProject/Materials/SkyBoxMaterial.h"
#include "ModelComponent.h"

SkyBoxPrefab::SkyBoxPrefab()
	:m_pBox(nullptr)
{
}


SkyBoxPrefab::~SkyBoxPrefab()
{
}

void SkyBoxPrefab::Initialize(const GameContext& gameContext)
{
	m_pBox = new ModelComponent(L"Resources/Meshes/Box.ovm");
	auto skyBox = new SkyBoxMaterial();
	skyBox->SetSkyBoxTexture(L"./Resources/Textures/SkyBox.dds");
	gameContext.pMaterialManager->AddMaterial(skyBox,0);
	m_pBox->SetMaterial(0);

	AddComponent(m_pBox);
}



