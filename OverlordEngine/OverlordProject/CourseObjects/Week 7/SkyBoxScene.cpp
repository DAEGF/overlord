#include "stdafx.h"
#include "SkyBoxScene.h"
#include "SkyBoxPrefab.h"

SkyBoxScene::SkyBoxScene() :
	GameScene(L"SkyboxScene")
{}

void SkyBoxScene::Initialize()
{
	//TODO add skybox
	AddChild(new SkyBoxPrefab());
}

void SkyBoxScene::Update()
{}

void SkyBoxScene::Draw()
{}
