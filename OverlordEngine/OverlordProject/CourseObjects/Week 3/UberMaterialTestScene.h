#pragma once
#include "GameScene.h"

class UberMaterialTestScene :
	public GameScene
{
public:
	UberMaterialTestScene(void);
	virtual ~UberMaterialTestScene(void) = default;

protected:

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;

private:

	GameObject * m_TeaPot;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	UberMaterialTestScene(const UberMaterialTestScene &obj);
	UberMaterialTestScene& operator=(const UberMaterialTestScene& obj);
};

