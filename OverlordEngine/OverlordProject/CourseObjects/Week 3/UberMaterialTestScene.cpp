#include "stdafx.h"

#include "UberMaterialTestScene.h"
#include "GameObject.h"
#include "Components.h"
#include "ContentManager.h"
#include "PhysxManager.h"
#include "../OverlordProject/Materials/ColorMaterial.h"
#include "../OverlordProject/Materials/DiffuseMaterial.h"
#include <PhysxProxy.h>
#include "../OverlordProject/Materials/UberMaterial.h"

UberMaterialTestScene::UberMaterialTestScene(void) :
	GameScene(L"UberMaterialTestScene")

{
}

                                                                                                                                                                                                                           void UberMaterialTestScene::Initialize()
{
	const auto gameContext = GetGameContext();

	//GROUND PLANE
	//************
	GetPhysxProxy()->EnablePhysxDebugRendering(true);
	auto physX = PhysxManager::GetInstance()->GetPhysics();

	auto bouncyMaterial = physX->createMaterial(0, 0, 1);
	auto ground = new GameObject();
	ground->AddComponent(new RigidBodyComponent(true));

	std::shared_ptr<physx::PxGeometry> geom(new physx::PxPlaneGeometry());
	ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, physx::PxTransform(physx::PxQuat(DirectX::XM_PIDIV2, physx::PxVec3(0, 0, 1)))));
	AddChild(ground);

	m_TeaPot = new GameObject();
	auto pTeaPotModel = new ModelComponent(L"Resources/Meshes/Teapot.ovm");

	m_TeaPot->AddComponent(pTeaPotModel);

	auto pUberMaterial = new UberMaterial();

	gameContext.pMaterialManager->AddMaterial(pUberMaterial, 0);
	pUberMaterial->SetDiffuseTexture(L"Resources/Textures/CobbleStone_DiffuseMap.dds");
	pUberMaterial->SetEnvironmentCube(L"Resources/Textures/Sunol_Cubemap.dds");
	pUberMaterial->SetNormalMapTexture(L"Resources/Textures/CobbleStone_NormalMap.dds");
	pUberMaterial->SetSpecularLevelTexture(L"Resources/Textures/CobbleStone_HeightMap.dds");
	pUberMaterial->SetOpacityTexture(L"Resources/Textures/Specular_Level.jpg");
	m_TeaPot->GetComponent<ModelComponent>()->SetMaterial(0);

	pUberMaterial->EnableEnvironmentMapping(true);
	pUberMaterial->EnableDiffuseTexture(true);
	pUberMaterial->EnableNormalMapping(false);
	pUberMaterial->EnableFresnelFaloff(false);
	pUberMaterial->EnableSpecularPhong(true);
	pUberMaterial->EnableSpecularBlinn(false);
	pUberMaterial->EnableOpacityMap(false);
	pUberMaterial->SetReflectionStrength(0.5);

	m_TeaPot->GetComponent<TransformComponent>()->Translate(0.f, 10.f, 1.f);


	// Build and Run

	AddChild(m_TeaPot);

}

void UberMaterialTestScene::Update()
{

}

void UberMaterialTestScene::Draw()
{
}
