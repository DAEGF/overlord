#include "stdafx.h"
#include "SpriteTestScene.h"
#include "GameObject.h"
#include "ContentManager.h"
#include "SpriteComponent.h"
#include "TransformComponent.h"
#include "OverlordGame.h"

#define FPS_COUNTER 1

SpriteTestScene::SpriteTestScene() :
GameScene(L"SpriteTestScene"),
m_FpsInterval(FPS_COUNTER),
m_Counter(105.0f),
m_pObj(nullptr)
{}

void SpriteTestScene::Initialize()
{
		//WINDOW SIZES
	auto windowWidth = OverlordGame::GetGameSettings().Window.Width;
	auto windowHeight = OverlordGame::GetGameSettings().Window.Height;

	//SET LOCATIONS
	m_MenuItemsPos.push_back(XMFLOAT2( 700.f, 350.f));
	m_MenuItemsPos.push_back(XMFLOAT2(windowWidth / 2.0f - 125.0f, windowHeight / 2.0f + 175.0f));

	m_pObj = new GameObject();
	m_pObj->AddComponent(new SpriteComponent(L"./Resources/Textures/Chair_Dark.dds", 
		DirectX::XMFLOAT2(0.5f, 0.5f), 
		DirectX::XMFLOAT4(1, 1, 1, 0.5f)));
	AddChild(m_pObj);

	m_pObj->GetTransform()->Translate(700.f, 350.f, .9f);
	m_pObj->GetTransform()->Scale(1.f, 2.f, 1.f);

	m_pTest = new GameObject();
	//auto startSprite = new SpriteComponent(L"./Resources/Textures/Start.png",DirectX::XMFLOAT2(0.5f, 0.5f), DirectX::XMFLOAT4(1, 1, 1, 0.5f));
	m_pTest->AddComponent( new SpriteComponent(L"./Resources/Textures/Start.png",
		DirectX::XMFLOAT2(0.5f, 0.5f), 
		DirectX::XMFLOAT4(1, 1, 1, 0.5f)));
	AddChild(m_pTest);
	
	m_pTest->GetTransform()->Translate(700.f, 350.f, .9f);
	m_pTest->GetTransform()->Scale(1.f, 2.f, 1.f);
	m_pTest->GetTransform()->Rotate(0, 0, 0.1f, false);

}

void SpriteTestScene::Update()
{
	const auto gameContext = GetGameContext();

	m_FpsInterval += gameContext.pGameTime->GetElapsed();
	if (m_FpsInterval >= FPS_COUNTER)
	{
		m_FpsInterval -= FPS_COUNTER;
		Logger::LogFormat(LogLevel::Info, L"FPS: %i", gameContext.pGameTime->GetFPS());
	}
	m_pObj->GetTransform()->Displace(1,0,0);
	//m_pTest->GetTransform()->Displace(1,0,0);
	m_pObj->GetTransform()->Rotate(0, 0, DirectX::XM_PIDIV2 * gameContext.pGameTime->GetTotal(), false);

}

void SpriteTestScene::Draw()
{}
