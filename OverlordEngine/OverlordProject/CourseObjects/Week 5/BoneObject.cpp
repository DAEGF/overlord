#include "stdafx.h"
#include "BoneObject.h"
#include "ModelComponent.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../../Materials/ColorMaterial.h"
#include "Components.h"


BoneObject::BoneObject(int boneId, int materialId, float length):
m_BoneId(boneId),
m_MaterialId(materialId),
m_Length(length)
{
}

void BoneObject::AddBone(BoneObject* pBone)
{
	pBone->GetTransform()->Translate(0,0,-m_Length);
	AddChild(pBone);
}

DirectX::XMFLOAT4X4 BoneObject::GetBindPose()
{
	return m_BindPose;
}

void BoneObject::CalculateBindPose()
{
	auto worldTransform = XMLoadFloat4x4(&GetTransform()->GetWorld());
	auto determinant = XMMatrixDeterminant(XMLoadFloat4x4(&GetTransform()->GetWorld()));
	auto inverse = XMMatrixInverse(&determinant, worldTransform);
	XMStoreFloat4x4(&m_BindPose, inverse);
	for(auto child : GetChildren<BoneObject>())
	{
		child->CalculateBindPose();
	}
}

void BoneObject::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto pChild = new GameObject();
	auto pModel = new ModelComponent(L"Resources/Meshes/Bone.ovm");
	pModel->SetMaterial(m_MaterialId);
	pChild->GetTransform()->Scale(m_Length,m_Length,m_Length);
	pChild->AddComponent(pModel);
	AddChild(pChild);


}

