#pragma once
#include <GameObject.h>
class BoneObject :
	public GameObject
{
public:
	BoneObject(int boneId, int materialId, float length = 5.f);
	~BoneObject() = default;

	BoneObject(const BoneObject& other) = delete;
	BoneObject(BoneObject&& other) noexcept = delete;
	BoneObject& operator=(const BoneObject& other) = delete;
	BoneObject& operator=(BoneObject&& other) = delete;

	void AddBone(BoneObject* pBone);
	DirectX::XMFLOAT4X4 GetBindPose();
	void CalculateBindPose();

protected:
	virtual void Initialize(const GameContext& gameContext) override;

private:
	float m_Length;
	int m_BoneId;
	int m_MaterialId;
	DirectX::XMFLOAT4X4 m_BindPose;
};

