#pragma once
#include "VertexHelper.h"

struct SkinnedVertex
{
	SkinnedVertex(DirectX::XMFLOAT3 pos, DirectX::XMFLOAT3 normal,
		DirectX::XMFLOAT4 color,float blendWeight0 = 1.0f, float blendWeight1 = 0):
	TransformedVertex(pos,normal,color),
	OriginalVertex(pos, normal, color),
	BlendWeight0(blendWeight0),
	BlendWeight1(blendWeight1)
	{}

	VertexPosNormCol TransformedVertex;
	VertexPosNormCol OriginalVertex;

	float BlendWeight0;
	float BlendWeight1;
};
