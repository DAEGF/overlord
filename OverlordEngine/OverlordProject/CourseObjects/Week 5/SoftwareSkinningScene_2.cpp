#include "stdafx.h"
#include "SoftwareSkinningScene_2.h"
#include "../../Materials/ColorMaterial.h"
#include "GameObject.h"
#include "../../CourseObjects/Week 5/BoneObject.h"
#include "Components.h"

SoftwareSkinningScene_2::SoftwareSkinningScene_2():
GameScene(L"SoftwareSkinningScene_2"),
m_pBone0(nullptr),
m_pBone1(nullptr),
m_BoneRotation(0),
m_RotationSign(1)
{

}

void SoftwareSkinningScene_2::Initialize()
{
	auto pMat = new ColorMaterial(true);
	GetGameContext().pMaterialManager->AddMaterial(pMat,0);

	auto root = new GameObject();
	m_pBone0 = new BoneObject(0,0,15.f);
	m_pBone1 = new BoneObject(1,0,15.f);
	root->AddChild(m_pBone0);
	m_pBone0->AddBone(m_pBone1);
	root->GetTransform()->Rotate(0,-90,0);
	AddChild(root);

	m_pBone0->CalculateBindPose();
	auto meshDrawer = new GameObject();
	m_pMeshDrawer = new MeshDrawComponent();
	meshDrawer->AddComponent(m_pMeshDrawer);
	CreateMesh(15.f);
	AddChild(meshDrawer);
}

void SoftwareSkinningScene_2::Update()
{
	m_BoneRotation += m_RotationSign * 45.f * GetGameContext().pGameTime->GetElapsed();

	if((m_RotationSign < 0) && m_BoneRotation <= -45) m_RotationSign = 1;
	if((m_RotationSign > 0) && m_BoneRotation >= 45) m_RotationSign = -1;

	m_pBone0->GetTransform()->Rotate(m_BoneRotation,0,0);
	m_pBone1->GetTransform()->Rotate(-m_BoneRotation*2.f,0,0);
	
	auto bindpose0 = m_pBone0->GetBindPose();
	auto boneTransform0 = DirectX::XMLoadFloat4x4(&bindpose0);
	boneTransform0 = XMMatrixMultiply(boneTransform0, XMLoadFloat4x4(&m_pBone0->GetTransform()->GetWorld()));
	
	auto bindpose1 = m_pBone1->GetBindPose();
	auto boneTransform1 = XMLoadFloat4x4(&bindpose1);
	boneTransform1 = XMMatrixMultiply(boneTransform1, XMLoadFloat4x4(&m_pBone1->GetTransform()->GetWorld()));

	for(size_t i = 0; i < m_SkinnedVertices.size(); i++)
	{
		DirectX::XMVECTOR transformed;
		if(i < 24)
		{
			transformed = XMVector3TransformCoord(XMLoadFloat3(&m_SkinnedVertices.at(i).OriginalVertex.Position), boneTransform0);
		}
		else
		{
			transformed = XMVector3TransformCoord(XMLoadFloat3(&m_SkinnedVertices.at(i).OriginalVertex.Position), boneTransform1);
		}
		XMStoreFloat3(&m_SkinnedVertices.at(i).TransformedVertex.Position, transformed);
	}

	m_pMeshDrawer->RemoveTriangles();
	for (size_t i = 0; i < m_SkinnedVertices.size(); i += 4)
	{
		QuadPosNormCol quad(m_SkinnedVertices.at(i).TransformedVertex,
			m_SkinnedVertices.at(i + 1).TransformedVertex,
			m_SkinnedVertices.at(i + 2).TransformedVertex,
			m_SkinnedVertices.at(i + 3).TransformedVertex);
		m_pMeshDrawer->AddQuad(quad);
	}
	m_pMeshDrawer->UpdateBuffer();
}

void SoftwareSkinningScene_2::Draw()
{

}

void SoftwareSkinningScene_2::CreateMesh(float length)
{
	auto pos = DirectX::XMFLOAT3(length / 2, 0, 0); 
	auto offset = DirectX::XMFLOAT3(length / 2, 2.5f, 2.5f); 
	auto col = DirectX::XMFLOAT4(1, 0, 0, 0.5f);
	//***** //BOX1* //*****
	//FRONT 
	auto norm = DirectX::XMFLOAT3(0, 0, -1); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	
	//BACK 
	norm = DirectX::XMFLOAT3(0, 0, 1); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col)); 
	
	//TOP
	norm = DirectX::XMFLOAT3(0, 1, 0); m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col));
	
	//BOTTOM 
	norm = DirectX::XMFLOAT3(0, -1, 0); m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col));
	
	//LEFT
	norm = DirectX::XMFLOAT3(-1, 0, 0); m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col)); 
	
	//RIGHT 
	norm = DirectX::XMFLOAT3(1, 0, 0); m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));	
	
	//***** //BOX2* //*****
	col = DirectX::XMFLOAT4(0, 1, 0, 0.5f); 
	pos = DirectX::XMFLOAT3(22.5f, 0, 0); 
	
	//FRONT 
	norm = DirectX::XMFLOAT3(0, 0, -1);
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
	
	//BACK
	norm = DirectX::XMFLOAT3(0, 0, 1);
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col)); 
	
	//TOP 
	norm = DirectX::XMFLOAT3(0, 1, 0); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	
	//BOTTOM 
	norm = DirectX::XMFLOAT3(0, -1, 0); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col)); 
	
	//LEFT 
	norm = DirectX::XMFLOAT3(-1, 0, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(-offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col)); 
	
	//RIGHT 
	norm = DirectX::XMFLOAT3(1, 0, 0); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, -offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, offset.y + pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, offset.z + pos.z), norm, col)); 
	m_SkinnedVertices.push_back(SkinnedVertex(DirectX::XMFLOAT3(offset.x + pos.x, -offset.y + pos.y, -offset.z + pos.z), norm, col));
}

