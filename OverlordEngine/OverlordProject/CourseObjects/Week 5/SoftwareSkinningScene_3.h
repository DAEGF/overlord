#pragma once
#include <GameScene.h>
#include "MeshDrawComponent.h"
#include "SkinnedVertex.h"

class BoneObject;

class SoftwareSkinningScene_3 :
	public GameScene
{
public:
	SoftwareSkinningScene_3();
	virtual ~SoftwareSkinningScene_3() = default;

	SoftwareSkinningScene_3(const SoftwareSkinningScene_3& other) = delete;
	SoftwareSkinningScene_3(SoftwareSkinningScene_3&& other) noexcept = delete;
	SoftwareSkinningScene_3& operator=(const SoftwareSkinningScene_3& other) = delete;
	SoftwareSkinningScene_3& operator=(SoftwareSkinningScene_3&& other) noexcept = delete;

protected:
	void Initialize() override;
	void Update() override;
	void Draw() override;

private:
	void CreateMesh(float length);

	BoneObject *m_pBone0,*m_pBone1;
	float m_BoneRotation;
	int m_RotationSign;
	MeshDrawComponent *m_pMeshDrawer;
	std::vector<SkinnedVertex> m_SkinnedVertices;
};

