#include "stdafx.h"
#include "CharacterTestScene.h"
#include "ControllerComponent.h"
#include "SpherePrefab.h"
#include "Character.h"
#include "RigidBodyComponent.h"
#include "PhysxManager.h"
#include "ColliderComponent.h"

CharacterTestScene::CharacterTestScene()
	:GameScene(L"CharacterTestScene"),
	m_pCharacter(nullptr)
{
	
}

CharacterTestScene::~CharacterTestScene()
{
	
}

void CharacterTestScene::Initialize()
{
	//PhysX
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto bouncyMaterial = physX->createMaterial(0.5, 0.5, 1.0f);

	//GROUND 
	auto ground = new GameObject();
	ground->AddComponent(new RigidBodyComponent(true));

	std::shared_ptr<physx::PxGeometry> geom(new physx::PxPlaneGeometry());
	ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, 
		physx::PxTransform(physx::PxQuat(DirectX::XM_PIDIV2, physx::PxVec3(0, 0, 1)))));
	AddChild(ground);

	m_pCharacter = new Character();
	AddChild(m_pCharacter);
}

void CharacterTestScene::Update()
{
	
}

void CharacterTestScene::Draw()
{

}




