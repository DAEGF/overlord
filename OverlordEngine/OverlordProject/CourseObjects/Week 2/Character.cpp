
#include "stdafx.h"
#include "Character.h"
#include "Components.h"
#include "Prefabs.h"
#include "GameScene.h"
#include "PhysxManager.h"
#include "PhysxProxy.h"


Character::Character(float radius, float height, float moveSpeed) : 
	m_Radius(radius),
	m_Height(height),
	m_MoveSpeed(moveSpeed),
	m_pCamera(nullptr),
	m_pController(nullptr),
	m_TotalPitch(0), 
	m_TotalYaw(0),
	m_RotationSpeed(90.f),
	//Running
	m_MaxRunVelocity(50.0f), 
	m_TerminalVelocity(20),
	m_Gravity(9.81f), 
	m_RunAccelerationTime(0.3f), 
	m_JumpAccelerationTime(0.8f), 
	m_RunAcceleration(m_MaxRunVelocity/m_RunAccelerationTime), 
	m_JumpAcceleration(m_Gravity/m_JumpAccelerationTime), 
	m_RunVelocity(0), 
	m_JumpVelocity(0),
	m_Velocity(0,0,0)
{}

void Character::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto physX = PhysxManager::GetInstance()->GetPhysics();

	auto pBouncyMaterial = physX->createMaterial(0, 0, 1);
	//TODO: Create controller
	m_pController = new ControllerComponent(pBouncyMaterial);
	AddComponent(m_pController);
	//TODO: Add a fixed camera as child
	auto camera = new FixedCamera();
	AddChild(camera);
	m_pCamera = camera->GetComponent<CameraComponent>();
	//TODO: Register all Input Actions
	gameContext.pInput->AddInputAction(InputAction(LEFT, Down, 'A'));
	gameContext.pInput->AddInputAction(InputAction(RIGHT, Down, 'D'));
	gameContext.pInput->AddInputAction(InputAction(FORWARD, Down, 'W'));
	gameContext.pInput->AddInputAction(InputAction(BACKWARD, Down, 'S'));
	gameContext.pInput->AddInputAction(InputAction(JUMP, Pressed, 'K'));
}

void Character::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//TODO: Set the camera as active
	m_pCamera = GetChild<FixedCamera>()->GetComponent<CameraComponent>();
	m_pCamera->SetActive();
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
}

void Character::Update(const GameContext& gameContext)
{
	using namespace DirectX;
	UNREFERENCED_PARAMETER(gameContext);
	//TODO: Update the character (Camera rotation, Character Movement, Character Gravity)
	XMFLOAT2 move = XMFLOAT2(0, 0);
	move.y = gameContext.pInput->IsActionTriggered(CharacterMovement::FORWARD) ? 1.0f : 0.0f;
	if (move.y == 0) move.y = -(gameContext.pInput->IsActionTriggered(CharacterMovement::BACKWARD) ? 1.0f : 0.0f);

	move.x = gameContext.pInput->IsActionTriggered(CharacterMovement::RIGHT) ? 1.0f : 0.0f;
	if (move.x == 0) move.x = -(gameContext.pInput->IsActionTriggered(CharacterMovement::LEFT) ? 1.0f : 0.0f);

	XMVECTOR forward = XMLoadFloat3(&GetTransform()->GetForward());
	XMVECTOR right = XMLoadFloat3(&GetTransform()->GetRight());

	auto direction = forward * move.y + right * move.x;

	if (move.x != 0.f || move.y != 0.f)
	{
		m_RunVelocity += m_RunAcceleration * gameContext.pGameTime->GetElapsed();
		if (m_RunVelocity > m_MaxRunVelocity) m_RunVelocity = m_MaxRunVelocity;

		auto tempVelocityY = m_Velocity.y;
		XMStoreFloat3(&m_Velocity, direction * m_RunVelocity);
		m_Velocity.y = tempVelocityY;
	}
	else
	{
		m_Velocity.x = 0.f;
		m_Velocity.z = 0.f;
	}

	if (m_pController->GetCollisionFlags() != physx::PxControllerCollisionFlag::eCOLLISION_DOWN)
	{
		m_JumpVelocity -= m_JumpAcceleration * gameContext.pGameTime->GetElapsed();
		if (m_JumpVelocity < -m_TerminalVelocity) m_JumpVelocity = -m_TerminalVelocity;
	}
	else if (gameContext.pInput->IsActionTriggered(CharacterMovement::JUMP))
	{
		m_JumpVelocity = 0.f;
		m_Velocity.y = 200.f;
	}
	else
	{
		m_Velocity.y = 0.f;
	}
	m_Velocity.y += m_JumpVelocity;


	//currPos += forward * move.y * currSpeed * gameContext.pGameTime->GetElapsed();
	//currPos += right * move.x * currSpeed * gameContext.pGameTime->GetElapsed();

	m_TotalYaw += move.x * m_RotationSpeed * gameContext.pGameTime->GetElapsed();
	//m_TotalPitch += move.y * m_RotationSpeed * gameContext.pGameTime->GetElapsed();

	auto controllerVel = XMFLOAT3(
		m_Velocity.x * gameContext.pGameTime->GetElapsed(),
		m_Velocity.y * gameContext.pGameTime->GetElapsed(),
		m_Velocity.z * gameContext.pGameTime->GetElapsed()
	);

	GetTransform()->Rotate(0, m_TotalYaw, 0);
	m_pController->Move(controllerVel);

}