#pragma once
#include "GameScene.h"

class ControllerComponent;

class CharacterTestScene : public GameScene
{
public:
	CharacterTestScene();
	virtual ~CharacterTestScene();

protected:
	void Initialize() override;
	void Update() override;
	void Draw() override;

private:
	GameObject * m_pCharacter;

private:
	CharacterTestScene(const CharacterTestScene& obj) = delete;
	CharacterTestScene operator=(const CharacterTestScene& obj) = delete;

};
