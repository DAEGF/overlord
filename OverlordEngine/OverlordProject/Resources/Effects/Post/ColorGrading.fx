//=============================================================================
//// Shader uses position and texture
//=============================================================================
SamplerState samPoint
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Mirror;
    AddressV = Mirror;
};

Texture2D gTexture;
Texture2D gLookUpTable;
static const float2 CLut_Size = float2(256.0, 16.0);

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};
/// Create Rasterizer State (Backface culling) 
RasterizerState BackCulling 
{ 
	CullMode = BACK; 
};

//IN/OUT STRUCTS
//--------------
struct VS_INPUT
{
    float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;

};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD1;
};


float3 ConvertToLUT(float3 originalColor)
{
    float2 CLut_pSize = 1.0 / CLut_Size;
    float4 CLut_UV;
    float3 color = saturate(originalColor) * (CLut_Size.y - 1.0);
    CLut_UV.w = floor(originalColor.b);
    CLut_UV.xy = (color.rg + 0.5) * CLut_pSize;
    CLut_UV.x += CLut_UV.w * CLut_pSize.y;
    CLut_UV.z = CLut_UV.x + CLut_pSize.y;
    return lerp(gLookUpTable.Sample(samPoint, CLut_UV.xy).rgb,
                 gLookUpTable.Sample(samPoint, CLut_UV.zy).rgb, color.b - CLut_UV.w);
};

//VERTEX SHADER
//-------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	// Set the Position
	output.Position = float4(input.Position,1.0f);
	// Set the TexCoord
	output.TexCoord = input.TexCoord;
	return output;
}


//PIXEL SHADER
//------------
float4 PS(PS_INPUT input): SV_Target
{
    float4 color = gTexture.Sample(samPoint, input.TexCoord);

    float3 gradedColor = ConvertToLUT(color.rgb);
    
    return float4(gradedColor.rgb, 1.0f);
}


//TECHNIQUE
//---------
technique11 ColorGrading
{
    pass P0
    {
		// Set states...
        SetDepthStencilState(EnableDepth, 0);
        SetRasterizerState(BackCulling);
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}