
#include "stdafx.h"

#include "ColorMaterial.h"

ID3DX11EffectVectorVariable* ColorMaterial::m_pColorVariable = nullptr;
ID3DX11EffectVectorVariable* ColorMaterial::m_pLightDirectionVariable = nullptr;

ColorMaterial::ColorMaterial(bool enableTransparency) : 
	Material(L"./Resources/Effects/PosNormCol3D.fx", enableTransparency?L"TransparencyTech":L"")
{}

void ColorMaterial::SetColor(DirectX::XMFLOAT4 color)
{
	m_Color = color;
}

void ColorMaterial::SetLightDirection(DirectX::XMFLOAT3 dir)
{
	m_LightDirection = dir;
}


void ColorMaterial::LoadEffectVariables()
{
}

void ColorMaterial::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);

}
