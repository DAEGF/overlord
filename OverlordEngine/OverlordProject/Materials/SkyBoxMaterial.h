#pragma once
#include "Material.h"

class TextureData;

class SkyBoxMaterial :
	public Material
{
public:
	SkyBoxMaterial();
	~SkyBoxMaterial();

	void SetSkyBoxTexture(const std::wstring& assetFile);

protected:
	void LoadEffectVariables() override;
	void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent) override;

private:
	TextureData* m_pSkyBoxTexture;
	static ID3DX11EffectShaderResourceVariable* m_pSkyBoxSRVvariable;

	SkyBoxMaterial(const SkyBoxMaterial &obj) = delete;
	SkyBoxMaterial& operator=(const SkyBoxMaterial& obj) = delete;


};

