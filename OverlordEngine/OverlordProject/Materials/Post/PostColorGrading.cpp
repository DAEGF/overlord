#include "stdafx.h"
#include "PostColorGrading.h"
#include "RenderTarget.h"
#include "TextureData.h"
#include "ContentManager.h"


PostColorGrading::PostColorGrading()
: PostProcessingMaterial(L"./Resources/Effects/Post/ColorGrading.fx", 0),
	m_pTextureMapVariable(nullptr),
	m_pLookUpMapVariable(nullptr)
	
{
}

void PostColorGrading::SetLookUpMapTexture(const std::wstring& assetFile)
{
	m_pLookupMap = ContentManager::Load<TextureData>(assetFile);
}

void PostColorGrading::LoadEffectVariables()
{
	if (!m_pTextureMapVariable)
	{
		m_pTextureMapVariable = GetEffect()->GetVariableByName("gTexture")->AsShaderResource();
		if (!m_pTextureMapVariable->IsValid())
		{
			Logger::LogWarning(L"PostColorGrading::LoadEffectVariables() > \'gTexture\' variable not found!");
			m_pTextureMapVariable = nullptr;
		}
	}

	if (!m_pLookUpMapVariable)
	{
		m_pLookUpMapVariable = GetEffect()->GetVariableByName("gLookUpTable")->AsShaderResource();
		if (!m_pLookUpMapVariable->IsValid())
		{
			Logger::LogWarning(L"PostColorGrading::LoadEffectVariables() > \'gLookUpTable\' variable not found!");
			m_pLookUpMapVariable = nullptr;
		}
	}
}

void PostColorGrading::UpdateEffectVariables(RenderTarget* pRendertarget)
{
	//TODO: Update the TextureMapVariable with the Color ShaderResourceView of the given RenderTarget
	if (pRendertarget && m_pTextureMapVariable)
	{
		m_pTextureMapVariable->SetResource(pRendertarget->GetShaderResourceView());
	}

	if(m_pLookupMap && m_pLookUpMapVariable)
	{
		m_pLookUpMapVariable->SetResource(m_pLookupMap->GetShaderResourceView());
	}
}

