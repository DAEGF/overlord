#include "stdafx.h"
#include "SkyBoxMaterial.h"
#include "ContentManager.h"
#include "TextureData.h"

ID3DX11EffectShaderResourceVariable* SkyBoxMaterial::m_pSkyBoxSRVvariable = nullptr;

SkyBoxMaterial::SkyBoxMaterial():
Material(L"./Resources/Effects/SkyBox.fx"),m_pSkyBoxTexture(nullptr)
{
}


SkyBoxMaterial::~SkyBoxMaterial()
{
}

void SkyBoxMaterial::SetSkyBoxTexture(const std::wstring& assetFile)
{
	m_pSkyBoxTexture = ContentManager::Load<TextureData>(assetFile);
}

void SkyBoxMaterial::LoadEffectVariables()
{
	if (!m_pSkyBoxSRVvariable)
	{
		m_pSkyBoxSRVvariable = GetEffect()->GetVariableByName("m_CubeMap")->AsShaderResource();
		if (!m_pSkyBoxSRVvariable->IsValid())
		{
			Logger::LogWarning(L"DiffuseMaterial::LoadEffectVariables() > \'m_Cubemap\' variable not found!");
			m_pSkyBoxSRVvariable = nullptr;
		}
	}
}

void SkyBoxMaterial::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);

	if (m_pSkyBoxTexture && m_pSkyBoxSRVvariable)
	{
		m_pSkyBoxSRVvariable->SetResource(m_pSkyBoxTexture->GetShaderResourceView());
	}
}
