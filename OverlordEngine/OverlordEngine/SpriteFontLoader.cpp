#include "stdafx.h"
#include "SpriteFontLoader.h"
#include "BinaryReader.h"
#include "ContentManager.h"
#include "TextureData.h"

SpriteFont* SpriteFontLoader::LoadContent(const std::wstring& assetFile)
{
	auto pBinReader = new BinaryReader();
	pBinReader->Open(assetFile);

	if (!pBinReader->Exists())
	{
		Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Failed to read the assetFile!\nPath: \'%s\'", assetFile.c_str());
		return nullptr;
	}

	
	//See BMFont Documentation for Binary Layout
	//Parse the Identification bytes (B,M,F)
	char b = pBinReader->Read<char>();
	char m = pBinReader->Read<char>();
	char f = pBinReader->Read<char>();
	//If Identification bytes doesn't match B|M|F,
	if (b != 'B' || m != 'M' || f != 'F')
	{
		//Log Error (SpriteFontLoader::LoadContent > Not a valid .fnt font) &
		//return nullptr
		Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Not a valid .fnt font ");
		return nullptr;
	}


	//Parse the version (version 3 required)
	char version = pBinReader->Read<char>();

	//If version is < 3,
	if(version < 3)
	{
		//Log Error (SpriteFontLoader::LoadContent > Only .fnt version 3 is supported)
		//return nullptr
	
		Logger::LogFormat(LogLevel::Error,L"SpriteFontLoader::LoadContent > Only .fnt version 3 is supported");
		return nullptr;
	}

	//Valid .fnt file
	auto pSpriteFont = new SpriteFont();
	//SpriteFontLoader is a friend class of SpriteFont
	//That means you have access to its privates (pSpriteFont->m_FontName = ... is valid)

	//**********
	// BLOCK 0 *
	//**********
	//Retrieve the blockId and blockSize
	char blockId = pBinReader->Read<char>();
	int blockSize = pBinReader->Read<int>();
	//Retrieve the FontSize (will be -25, BMF bug) [SpriteFont::m_FontSize]
	pSpriteFont->m_FontSize = pBinReader->Read<short>();
	//Move the binreader to the start of the FontName [BinaryReader::MoveBufferPosition(...) or you can set its position using BinaryReader::SetBufferPosition(...))
	pBinReader->MoveBufferPosition(12);
	//Retrieve the FontName [SpriteFont::m_FontName]
	pSpriteFont->m_FontName = pBinReader->ReadNullString();
	//...


	//**********
	// BLOCK 1 *
	//**********
	//Retrieve the blockId and blockSize
	blockId = pBinReader->Read<char>();
	blockSize = pBinReader->Read<int>();
	////Retrieve Texture Width & Height [SpriteFont::m_TextureWidth/m_TextureHeight]
	pBinReader->MoveBufferPosition(4);
	pSpriteFont->m_TextureWidth = pBinReader->Read<short>();
	pSpriteFont->m_TextureHeight = pBinReader->Read<short>();
	//Retrieve PageCount
	int pagecount = pBinReader->Read<short>();
	//> if pagecount > 1
	if(pagecount >1)
	{
		Logger::LogFormat(LogLevel::Error,L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed");
	}
	//> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed)
	pBinReader->MoveBufferPosition(5);
	//Advance to Block2 (Move Reader)
	//...

	//**********
	// BLOCK 2 *
	//**********
	//Retrieve the blockId and blockSize
	blockId = pBinReader->Read<char>();
	blockSize = pBinReader->Read<int>();
	//Retrieve the PageName (store Local)
	auto pageName = pBinReader->ReadNullString();
	//	> If PageName is empty
	if (pageName.empty())
	{
		Logger::LogFormat(LogLevel::Error,L"SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty]");
	}
	//>Retrieve texture filepath from the assetFile path
	auto found = assetFile.rfind('/');
	std::wstring filePath = assetFile;
	filePath = filePath.substr(0,found);
	filePath += L"/" + pageName;
	//> (ex. c:/Example/somefont.fnt => c:/Example/) [Have a look at: wstring::rfind()]
	//>Use path and PageName to load the texture using the ContentManager [SpriteFont::m_pTexture]
	pSpriteFont->m_pTexture = ContentManager::Load<TextureData>(filePath);
	//> (ex. c:/Example/ + 'PageName' => c:/Example/somefont_0.png)
	//...

	//**********
	// BLOCK 3 *
	//**********
	//Retrieve the blockId and blockSize
	blockId = pBinReader->Read<char>();
	blockSize = pBinReader->Read<int>();
	//Retrieve Character Count (see documentation)
	auto charCount = blockSize/20;
	//Retrieve Every Character, For every Character:
	for (int i {}; i < charCount; i++)
	{
		//> Retrieve CharacterId (store Local) and cast to a 'wchar_t'
		wchar_t charId = wchar_t(pBinReader->Read<int>());
	
		//> Check if CharacterId is valid (SpriteFont::IsCharValid), Log Warning and advance to next character if not valid
		if(!pSpriteFont->IsCharValid(charId))
		{
			Logger::LogWarning(L"Character ID not valid!");
		}
		else
		{
			
		//> Retrieve the corresponding FontMetric (SpriteFont::GetMetric) [REFERENCE!!!]
			auto fontMetric = &pSpriteFont->GetMetric(charId);
		//> Set IsValid to true [FontMetric::IsValid]
			fontMetric->IsValid = true;
	
		//> Set Character (CharacterId) [FontMetric::Character]
			fontMetric->Character = charId;
	
		//> Retrieve Xposition (store Local)
			short xPos = pBinReader->Read<short>(); 
		//> Retrieve Yposition (store Local)
			short yPos = pBinReader->Read<short>();
		//> Retrieve & Set Width [FontMetric::Width]
			fontMetric->Width = pBinReader->Read<short>();
		//> Retrieve & Set Height [FontMetric::Height]
			fontMetric->Height = pBinReader->Read<short>();
		//> Retrieve & Set OffsetX [FontMetric::OffsetX]
			fontMetric->OffsetX = pBinReader->Read<short>();
		//> Retrieve & Set OffsetY [FontMetric::OffsetY]
			fontMetric->OffsetY = pBinReader->Read<short>();
		//> Retrieve & Set AdvanceX [FontMetric::AdvanceX]
			fontMetric->AdvanceX = pBinReader->Read<short>();
		//> Retrieve & Set Page [FontMetric::Page]
			fontMetric->Page = pBinReader->Read<char>();
	
		//> Retrieve Channel (BITFIELD!!!) 
			auto channel = pBinReader->Read<char>();
	
		//	> See documentation for BitField meaning [FontMetrix::Channel]
			if (channel == 1) fontMetric->Channel = 2;
			else if (channel == 2) fontMetric->Channel = 1;
			else if (channel == 4) fontMetric->Channel = 0;
			else if (channel == 8) fontMetric->Channel = 3;
	
		//> Calculate Texture Coordinates using Xposition, Yposition, TextureWidth & TextureHeight [FontMetric::TexCoord]
			auto textSize = pSpriteFont->m_pTexture->GetDimension();
			fontMetric->TexCoord = DirectX::XMFLOAT2(float(xPos / textSize.x), float(yPos / textSize.y));		
		}

	}
	
	//...

	//DONE :)

	delete pBinReader;
	return pSpriteFont;
}

void SpriteFontLoader::Destroy(SpriteFont* objToDestroy)
{
	SafeDelete(objToDestroy);
}
