#pragma once
//#include "stdafx.h"

inline DirectX::XMVECTOR ToXMVector(const DirectX::XMFLOAT3& val)
{
	return XMLoadFloat3(&val);	
}

inline DirectX::XMFLOAT3 ToXMFloat3(const DirectX::XMVECTOR& vec)
{
	DirectX::XMFLOAT3 val;
	XMStoreFloat3(&val, vec);
	return val;
}