#pragma once
#include "BaseComponent.h"

class TransformComponent : public BaseComponent
{
	DirectX::XMFLOAT3 m_Position, m_WorldPosition, m_Scale, m_WorldScale, m_Forward, m_Up, m_Right;
	DirectX::XMFLOAT4 m_Rotation, m_WorldRotation;
	DirectX::XMFLOAT4X4 m_World {};
	unsigned char m_IsTransformChanged;

public:
	TransformComponent(const TransformComponent& other) = delete;
	TransformComponent(TransformComponent&& other) noexcept = delete;
	TransformComponent& operator=(const TransformComponent& other) = delete;
	TransformComponent& operator=(TransformComponent&& other) noexcept = delete;
	TransformComponent();
	virtual ~TransformComponent() = default;

	void Translate(float x, float y, float z);
	void Translate(const DirectX::XMFLOAT3& position);
	void Translate(const DirectX::XMVECTOR& position);

	void Displace(float x, float y, float z);
	void Displace(const DirectX::XMFLOAT3& displacement);
	void Displace(const DirectX::XMVECTOR& displacement);

	void Rotate(float x, float y, float z, bool isEuler = true);
	void Rotate(const DirectX::XMFLOAT3& rotation, bool isEuler = true);
	void Rotate(const DirectX::XMVECTOR& rotation, bool isQuaternion = true);

	void Scale(float x, float y, float z);
	void Scale(const DirectX::XMFLOAT3& scale);

	void LookAt(const DirectX::XMFLOAT3& target, float slerpPercent = .05f, bool useWorldUp = true, bool invertRotate = true);
	void LookTowards(const DirectX::XMFLOAT3& direction, float slerpPercent = .05f, bool useWorldUp = true, bool invertRotate = true);

	const DirectX::XMFLOAT3& GetPosition() const { return m_Position; }
	const DirectX::XMFLOAT3& GetWorldPosition() const { return m_WorldPosition; }
	const DirectX::XMFLOAT3& GetScale() const { return m_Scale; }
	const DirectX::XMFLOAT3& GetWorldScale() const { return m_WorldScale; }
	const DirectX::XMFLOAT4& GetRotation() const { return m_Rotation; }
	const DirectX::XMFLOAT4& GetWorldRotation() const { return m_WorldRotation; }
	const DirectX::XMFLOAT4X4& GetWorld() const { return m_World; }

	const DirectX::XMFLOAT3& GetForward() const { return m_Forward; }
	const DirectX::XMFLOAT3& GetUp() const { return m_Up; }
	const DirectX::XMFLOAT3& GetRight() const { return m_Right; }

	
	void SetTransformFlag(unsigned char transformChanged) { m_IsTransformChanged |= transformChanged;}

protected:

	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;
	void Draw(const GameContext& gameContext) override;

	void UpdateTransforms();
	bool CheckConstraints() const;
};
