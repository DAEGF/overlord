#pragma once

inline bool UpdateAndCheckTimer(float& timer,const float& max,float elapsed)
{
	timer += elapsed;
	if(timer > max)
	{
		timer = 0.f;
		return true;
	}
	return false;
}
