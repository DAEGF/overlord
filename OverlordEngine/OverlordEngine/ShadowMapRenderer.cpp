#include "stdafx.h"
#include "ShadowMapRenderer.h"
#include "ContentManager.h"
#include "ShadowMapMaterial.h"
#include "RenderTarget.h"
#include "MeshFilter.h"
#include "SceneManager.h"
#include "OverlordGame.h"

ShadowMapRenderer::~ShadowMapRenderer()
{
	//TODO: make sure you don't have memory leaks and/or resource leaks :) -> Figure out if you need to do something heres
	delete m_pShadowMat;
    delete m_pShadowRT;
}

void ShadowMapRenderer::Initialize(const GameContext& gameContext)
{
	if (m_IsInitialized)
		return;

	UNREFERENCED_PARAMETER(gameContext);
	//TODO: create shadow generator material + initialize it
	m_pShadowMat = new ShadowMapMaterial();
	m_pShadowMat->Initialize(gameContext);
	//TODO: create a rendertarget with the correct settings (hint: depth only) for the shadow generator using a RENDERTARGET_DESC
	m_pShadowRT = new RenderTarget(gameContext.pDevice);
	RENDERTARGET_DESC renderTargetDesc{};
	renderTargetDesc.EnableDepthSRV = true;
	renderTargetDesc.EnableColorBuffer = false;
	renderTargetDesc.EnableDepthBuffer = true;
	renderTargetDesc.EnableColorSRV= false;
	renderTargetDesc.Height = OverlordGame::GetGameSettings().Window.Height;
	renderTargetDesc.Width = OverlordGame::GetGameSettings().Window.Width;

	m_pShadowRT->Create(renderTargetDesc);
	m_IsInitialized = true;
}

void ShadowMapRenderer::SetLight(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 direction)
{
	UNREFERENCED_PARAMETER(position);
	UNREFERENCED_PARAMETER(direction);
	//TODO: store the input parameters in the appropriate datamembers
	m_LightPosition = position;
	m_LightDirection = direction;
	//TODO: calculate the Light VP matrix (Directional Light only ;)) and store it in the appropriate datamember
	auto lightPositionVec = DirectX::XMLoadFloat3(&m_LightPosition);
	auto lightDirectionVec = DirectX::XMLoadFloat3(&m_LightDirection);

	
    DirectX::XMVECTOR upVec = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f,1.0f);
    auto lookPos = DirectX::XMVectorAdd(lightPositionVec, lightDirectionVec);
    DirectX::XMMATRIX view = DirectX::XMMatrixLookAtLH(lightPositionVec,lookPos, upVec);


    float viewWidth = (m_Size > 0) ? m_Size * OverlordGame::GetGameSettings().Window.AspectRatio : OverlordGame::GetGameSettings().Window.Width;
    float viewHeight = (m_Size > 0) ? m_Size : OverlordGame::GetGameSettings().Window.Height;

    DirectX::XMMATRIX projection = DirectX::XMMatrixOrthographicLH(viewWidth, viewHeight, .001f, 205.f);
    DirectX::XMMATRIX viewProjection = view * projection;
    XMStoreFloat4x4(&m_LightVP, viewProjection);

}

void ShadowMapRenderer::Begin(const GameContext& gameContext) const
{
	//Reset Texture Register 5 (Unbind)
	ID3D11ShaderResourceView *const pSRV[] = { nullptr };
	gameContext.pDeviceContext->PSSetShaderResources(1, 1, pSRV);

	UNREFERENCED_PARAMETER(gameContext);
	//TODO: set the appropriate render target that our shadow generator will write to (hint: use the OverlordGame::SetRenderTarget function through SceneManager)
	SceneManager::GetInstance()->GetGame()->SetRenderTarget(m_pShadowRT);
	//TODO: clear this render target
	FLOAT color[4]{};
	m_pShadowRT->Clear(gameContext,color);
	//TODO: set the shader variables of this shadow generator material
	m_pShadowMat->SetLightVP(m_LightVP);
}

void ShadowMapRenderer::End(const GameContext& gameContext) const
{
	UNREFERENCED_PARAMETER(gameContext);
	//TODO: restore default render target (hint: passing nullptr to OverlordGame::SetRenderTarget will do the trick)
	SceneManager::GetInstance()->GetGame()->SetRenderTarget(nullptr);
}

void ShadowMapRenderer::Draw(const GameContext& gameContext, MeshFilter* pMeshFilter, DirectX::XMFLOAT4X4 world, const std::vector<DirectX::XMFLOAT4X4>& bones) const
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pMeshFilter);
	UNREFERENCED_PARAMETER(world);
	UNREFERENCED_PARAMETER(bones);
	
	VertexBufferData vertBufferData{};
    int type{ pMeshFilter->m_HasAnimations };
    ID3DX11EffectTechnique* tech{nullptr};
    
    tech = m_pShadowMat->m_pShadowTechs[type];
    vertBufferData = pMeshFilter->GetVertexBufferData(gameContext, m_pShadowMat->m_InputLayoutIds[type]);

    if (type)
        m_pShadowMat->SetBones(&bones[0]._11, bones.size());
    m_pShadowMat->SetWorld(world);
    m_pShadowMat->SetLightVP(m_LightVP);

    //TODO: set the correct inputlayout, buffers, topology (some variables are set based on the generation type Skinned or Static)
    UINT offset = 0;
    gameContext.pDeviceContext->IASetInputLayout(m_pShadowMat->m_pInputLayouts[type]);
    gameContext.pDeviceContext->IASetVertexBuffers(0, 1, &vertBufferData.pVertexBuffer, &vertBufferData.VertexStride, &offset);
    gameContext.pDeviceContext->IASetIndexBuffer(pMeshFilter->m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
    gameContext.pDeviceContext->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    //TODO: invoke draw call
    D3DX11_TECHNIQUE_DESC techDesc;
    tech->GetDesc(&techDesc);
    for (UINT p = 0; p < techDesc.Passes; ++p)
    {
        tech->GetPassByIndex(p)->Apply(0, gameContext.pDeviceContext);
        gameContext.pDeviceContext->DrawIndexed(pMeshFilter->m_IndexCount, 0, 0);
    }
	
}

void ShadowMapRenderer::UpdateMeshFilter(const GameContext& gameContext, MeshFilter* pMeshFilter)
{
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pMeshFilter);
	//TODO: based on the type (Skinned or Static) build the correct vertex buffers for the MeshFilter (Hint use MeshFilter::BuildVertexBuffer)
	int type =int(pMeshFilter->m_HasAnimations);
	pMeshFilter->BuildVertexBuffer(gameContext,m_pShadowMat->m_InputLayoutIds[type],
		m_pShadowMat->m_InputLayoutSizes[type],m_pShadowMat->m_InputLayoutDescriptions[type]);
}

ID3D11ShaderResourceView* ShadowMapRenderer::GetShadowMap() const
{
	//TODO: return the depth shader resource view of the shadow generator render target
	return m_pShadowRT->GetDepthShaderResourceView();
}
