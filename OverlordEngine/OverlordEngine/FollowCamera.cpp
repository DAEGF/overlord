#include "stdafx.h"
#include "FollowCamera.h"
#include "Components.h"
#include "MathHelper.h"


using namespace DirectX;

FollowCamera::FollowCamera(float moveSpeed, float distance, float height, float rotDamping, float posDamping, float heightDamping):
	m_MoveSpeed(moveSpeed),
	m_Distance(distance),
	m_Height(height),
	m_PositionDamping(posDamping),
	m_HeightDamping(heightDamping),
	m_RotationDamping(rotDamping),
	m_pTarget(nullptr),
	m_pCamera(nullptr)
{
}

void FollowCamera::AddTarget(GameObject* target)
{
	m_pTarget = target;
}

void FollowCamera::AddTargetPos(GameObject* targetPos)
{
	m_pTargetPosition = targetPos;
}

void FollowCamera::UpdateTargetVelocity(DirectX::XMFLOAT3 vel)
{
	m_TargetVelocity = vel;
}

void FollowCamera::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pCamera = new CameraComponent();
	AddComponent(m_pCamera);

	//m_pTargetCameraPos->GetTransform()->Translate(0,m_Height,-m_Distance);
}

void FollowCamera::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if (!m_pTarget) return;
	
	// Get Wantedposition (target is parented to character)
	auto wantedPos = CalculateTargetPosition();
	//get currentPosition 
	auto currentPos = GetTransform()->GetWorldPosition();

	auto lerpedHeight = Lerp(GetTransform()->GetPosition().y,m_Height + m_pTarget->GetTransform()->GetPosition().y, 
		m_HeightDamping * gameContext.pGameTime->GetElapsed());
	// Damp the height
	auto lerpedPos = Lerp(currentPos, wantedPos, m_PositionDamping * gameContext.pGameTime->GetElapsed());

	// Set the position of the camera on the x-z plane to:
	// distance meters behind the target
	GetTransform()->Translate(lerpedPos.x,lerpedHeight, lerpedPos.z);
	
	// Always look at the target
	GetTransform()->LookAt(m_pTarget->GetTransform()->GetPosition(),m_RotationDamping * gameContext.pGameTime->GetElapsed());
}

XMFLOAT3 FollowCamera::CalculateTargetPosition() const
{
	return  m_pTargetPosition->GetTransform()->GetWorldPosition();
}


