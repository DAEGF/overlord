#pragma once
#include "GameObject.h"
class FollowCamera :
	public GameObject
{
public:
	FollowCamera(float moveSpeed, float distance, float rotDamping , float height, float posDamping, float heightDamping);
	virtual ~FollowCamera() = default;
	FollowCamera(const FollowCamera& other) = delete;
	FollowCamera(FollowCamera&& other) noexcept = delete;
	FollowCamera& operator=(const FollowCamera& other) = delete;
	FollowCamera& operator=(FollowCamera&& other) noexcept = delete;

	void AddTarget(GameObject* target);
	void AddTargetPos(GameObject* targetPos);
	void UpdateTargetVelocity(XMFLOAT3 vel);

	float GetDistance() const {return m_Distance;}
	float GetHeight() const {return m_Height;}
protected:
	void Initialize(const GameContext& gameContext) override;
	void Update(const GameContext& gameContext) override;

private:
	float m_MoveSpeed = 10.0f;
	
	float m_Distance = 10.0f;
	float m_Height = 5.0f;
	float m_PositionDamping = 2.0f;
	float m_HeightDamping = 3.0f;
	float m_RotationDamping = 5.0f;

	GameObject* m_pTarget;
	GameObject* m_pTargetPosition;
	CameraComponent* m_pCamera;
	XMFLOAT3 m_TargetVelocity{};

	XMFLOAT3 CalculateTargetPosition() const;
};

