#include "stdafx.h"
#include "ModelAnimator.h"

ModelAnimator::ModelAnimator(MeshFilter* pMeshFilter):
	m_pMeshFilter(pMeshFilter),
	m_Transforms(std::vector<XMFLOAT4X4>()),
	m_IsPlaying(false),
	m_Reversed(false),
	m_ClipSet(false),
	m_TickCount(0),
	m_AnimationSpeed(1.0f),
	m_PreviousKey()
{
	SetAnimation(0, true);
	m_PreviousKey = m_CurrentClip.Keys[0];
}

void ModelAnimator::SetAnimation(UINT clipNumber, bool shouldLoop)
{
	UNREFERENCED_PARAMETER(clipNumber);
	//TODO: complete
	//Set m_ClipSet to false
	m_ClipSet = false;
	//Check if clipNumber is smaller than the actual m_AnimationClips vector size
	if (clipNumber >= m_pMeshFilter->m_AnimationClips.size())
	{
		//If not,
		//	Call Reset
		//	Log a warning with an appropriate message
		//	return
		Reset(false);
		Logger::Log(Warning, L"clipnumber is out of bounds for clips vector");
		return;
	}
	//else
	//	Retrieve the AnimationClip from the m_AnimationClips vector based on the given clipNumber
	//	Call SetAnimation(AnimationClip clip)
	auto clip = m_pMeshFilter->m_AnimationClips[clipNumber];
	m_IdxClipSet = clipNumber;
	SetAnimation(clip, shouldLoop);
}

void ModelAnimator::SetAnimation(std::wstring clipName, bool shouldLoop)
{
	UNREFERENCED_PARAMETER(clipName);
	//TODO: complete
	//Set m_ClipSet to false
	m_ClipSet = false;
	//Iterate the m_AnimationClips vector and search for an AnimationClip with the given name (clipName)
	for (UINT i = 0; i < m_pMeshFilter->m_AnimationClips.size(); i++)
	{
		auto clip = m_pMeshFilter->m_AnimationClips[i];
		//If found,
		//	Call SetAnimation(Animation Clip) with the found clip
		if (clip.Name == clipName)
		{
			m_IdxClipSet = i;
			SetAnimation(clip, shouldLoop);
			return;
		}
	}

	//Else
	//	Call Reset
	Reset(false);
	//	Log a warning with an appropriate message
	Logger::Log(Warning, L"clip " + clipName + L" not Found");
}

void ModelAnimator::SetAnimation(AnimationClip clip, bool shouldLoop)
{
	UNREFERENCED_PARAMETER(clip);
	//TODO: complete
	m_IsLooping = shouldLoop;
	//Set m_ClipSet to true
	m_ClipSet = true;
	//Set m_CurrentClip
	m_CurrentClip = clip;
	//Call Reset(false)
	Reset(false);
}

void ModelAnimator::Reset(bool pause)
{
	UNREFERENCED_PARAMETER(pause);
	//TODO: complete
	//If pause is true, set m_IsPlaying to false
	if (pause) m_IsPlaying = false;
	//Set m_TickCount to zero
	m_TickCount = 0;
	//Set m_AnimationSpeed to 1.0f
	m_AnimationSpeed = 1.0f;

	m_IsDone = false;

	//If m_ClipSet is true
	if (m_ClipSet)
	{
		//	Retrieve the BoneTransform from the first Key from the current clip (m_CurrentClip)
		auto transforms = m_CurrentClip.Keys[0].BoneTransforms;
		//	Refill the m_Transforms vector with the new BoneTransforms (have a look at vector::assign)
		m_Transforms.assign(transforms.size(), transforms.front());
	}
	else
	{
		//Else
		//	Create an IdentityMatrix 
		//	Refill the m_Transforms vector with this IdenityMatrix (Amount = BoneCount) (have a look at vector::assign)
		XMFLOAT4X4 transform{};
		XMStoreFloat4x4(&transform, XMMatrixIdentity());
		m_Transforms.assign(m_pMeshFilter->m_BoneCount, transform);
	}
}

void ModelAnimator::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//TODO: complete
	//We only update the transforms if the animation is running and the clip is set
	if (m_IsPlaying && m_ClipSet && gameContext.pGameTime->IsRunning())
	{
		//1. 
		//Calculate the passedTicks (see the lab document)
		auto passedTicks = gameContext.pGameTime->GetElapsed() * m_CurrentClip.TicksPerSecond * m_AnimationSpeed;
		//auto passedTicks = ...
		//Make sure that the passedTicks stay between the m_CurrentClip.Duration bounds (fmod)
		passedTicks = fmod(passedTicks, m_CurrentClip.Duration);
		//2. 
		//IF m_Reversed is true
		if (m_Reversed)
		{
			//	Subtract passedTicks from m_TickCount
			//	If m_TickCount is smaller than zero, add m_CurrentClip.Duration to m_TickCount
			m_TickCount -= passedTicks;
			if (m_TickCount < 0)
			{
				m_TickCount += m_CurrentClip.Duration;
				if (!m_IsLooping) m_IsDone = true;
			}
		}
		else
		{
			//ELSE
			//	Add passedTicks to m_TickCount
			m_TickCount += passedTicks;
			//	if m_TickCount is bigger than the clip duration, subtract the duration from m_TickCount
			if (m_TickCount > m_CurrentClip.Duration)
			{
				m_TickCount -= m_CurrentClip.Duration;
				if (!m_IsLooping) 
					m_IsDone = true;
			}
		}

		//3.
		//Find the enclosing keys
		AnimationKey keyA, keyB;
		keyA.Tick = 0;
		keyB.Tick = m_CurrentClip.Duration;
		//Iterate all the keys of the clip and find the following keys:
		//keyA > Closest Key with Tick before/smaller than m_TickCount
		//keyB > Closest Key with Tick after/bigger than m_TickCount
		for (auto key : m_CurrentClip.Keys)
		{
			if (key.Tick < m_TickCount && key.Tick >= keyA.Tick) keyA = key;
			if (key.Tick > m_TickCount && key.Tick <= keyB.Tick) keyB = key;
		}

		//4.
		//Interpolate between keys
		//Figure out the BlendFactor (See lab document)
		auto blendFactor = (m_TickCount - keyA.Tick) / (keyB.Tick - keyA.Tick);
		//Clear the m_Transforms vector
		m_Transforms.clear();
		//FOR every boneTransform in a key (So for every bone)
		for (unsigned int i = 0; i < keyB.BoneTransforms.size(); ++i)
		{
			//	Retrieve the transform from keyA (transformA)
			auto transformA = keyA.BoneTransforms[i];
			// 	Retrieve the transform from keyB (transformB)
			auto transformB = keyB.BoneTransforms[i];
			
			//	Decompose both transforms
			XMVECTOR translationA;
			XMVECTOR scaleA;
			XMVECTOR rotationA;
			XMMatrixDecompose(&scaleA, &rotationA, &translationA, XMLoadFloat4x4(&transformA));

			XMVECTOR translationB;
			XMVECTOR scaleB;
			XMVECTOR rotationB;
			XMMatrixDecompose(&scaleB, &rotationB, &translationB, XMLoadFloat4x4(&transformB));
			//	Lerp between all the transformations (Position, Scale, Rotation)
			auto blendTranslation = XMVectorLerp(translationA, translationB, blendFactor);
			auto blendRotation = XMQuaternionSlerp(rotationA, rotationB, blendFactor);
			auto blendScale = XMVectorLerp(scaleA, scaleB, blendFactor);
			//	Compose a transformation matrix with the lerp-results
			XMFLOAT4 scalef;
			XMStoreFloat4(&scalef, blendScale);

			XMFLOAT4 translationf;
			XMStoreFloat4(&translationf, blendTranslation);

			XMMATRIX transformMat = XMMatrixScaling(scalef.x, scalef.y, scalef.z) *
				XMMatrixRotationQuaternion(blendRotation) *
				XMMatrixTranslation(translationf.x, translationf.y, translationf.z);

			XMFLOAT4X4 finalTransform;
			XMStoreFloat4x4(&finalTransform, transformMat);
			//	Add the resulting matrix to the m_Transforms vector
			m_Transforms.push_back(finalTransform);
		}
	}
}
                   