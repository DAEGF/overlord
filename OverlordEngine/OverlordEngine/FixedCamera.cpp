#include "stdafx.h"
#include "FixedCamera.h"
#include "TransformComponent.h"

void FixedCamera::Initialize(const GameContext& )
{
	AddComponent(new CameraComponent());

	//TODO: remove magic transform/value
	GetTransform()->Translate(0,10,-10);
	GetTransform()->Rotate(45,0,0,true);
}

void FixedCamera::Update(const GameContext&)
{
}
