#include "stdafx.h"

#include "SpriteComponent.h"
 #include <utility>

#include "GameObject.h"
#include "TextureData.h"
#include "ContentManager.h"
#include "SpriteRenderer.h"
#include "TransformComponent.h"

SpriteComponent::SpriteComponent(std::wstring spriteAsset, DirectX::XMFLOAT2 pivot, DirectX::XMFLOAT4 color):
	m_pTexture(nullptr),
	m_SpriteAsset(std::move(spriteAsset)),
	m_Pivot(pivot),
	m_Color(color)
{
	m_pTexture = ContentManager::Load<TextureData>(m_SpriteAsset);
}

void SpriteComponent::Initialize(const GameContext& )
{
	m_pTexture = ContentManager::Load<TextureData>(m_SpriteAsset);
}

XMFLOAT2 SpriteComponent::GetDimensions() const
{
	if(!m_pTexture) return XMFLOAT2();
	return m_pTexture->GetDimension();
}

void SpriteComponent::SetTexture(const std::wstring& spriteAsset)
{
	m_SpriteAsset = spriteAsset;
	m_pTexture = ContentManager::Load<TextureData>(m_SpriteAsset);
}

void SpriteComponent::Update(const GameContext& )
{
}

void SpriteComponent::Draw(const GameContext& )
{
	if (!m_pTexture)
		return;

	auto renderer = SpriteRenderer::GetInstance();
	//TODO: Here you need to draw the SpriteComponent using the Draw of the sprite renderer
	// The sprite renderer is a singleton
	// you will need to position, the rotation and the scale
	// You can use the QuaternionToEuler function to help you with the z rotation 

	renderer->Draw(
		m_pTexture, XMFLOAT2(GetTransform()->GetPosition().x, GetTransform()->GetPosition().y), m_Color, m_Pivot,
		XMFLOAT2(GetTransform()->GetScale().x, GetTransform()->GetScale().y), 
		QuaternionToEuler(GetTransform()->GetRotation()).z, GetTransform()->GetPosition().z
	);
	
}
